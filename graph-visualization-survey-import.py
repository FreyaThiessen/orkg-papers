import requests
from orkg import ORKG

orkg = ORKG()
vocab = dict()

'''

Imports the graph-based visualization comparison

Data from: 
Bikakis, N., & Sellis, T. (2016). Exploration and visualization in the web of big 
linked data: A survey of the state of the art. arXiv preprint arXiv:1601.08059.

'''

def main():
    ############################# PROPERTIES AND RESOURCES #############################
    graphVisualizationProblem = orkg.resources.add(label='Graph-based visualization systems').content['id']

    # find or create implementation
    # TODO: make a generic function for this
    findImplementationPredicate = orkg.predicates.get(q='Implementation', exact=True).content

    if (len(findImplementationPredicate) > 0):
        implementationPredicate = findImplementationPredicate[0]['id']
    else:
        implementationPredicate = orkg.predicates.add(label='Implementation').content['id']

    keywordPredicate = orkg.predicates.add(label='Keyword').content['id']
    filterPredicate = orkg.predicates.add(label='Filter').content['id']
    samplingPredicate = orkg.predicates.add(label='Sampling').content['id']
    aggregationPredicate = orkg.predicates.add(label='Aggregation').content['id']
    incrementalPredicate = orkg.predicates.add(label='Incremental').content['id']
    diskPredicate = orkg.predicates.add(label='Disk').content['id']
    domainPredicate = orkg.predicates.add(label='Domain').content['id']
    applicationTypePredicate = orkg.predicates.add(label='Application type').content['id']

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "IsaViz: a Visual Environment for Browsing and Authoring RDF Models",
            "authors": [{"label": "E. Pietriga"}],
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "IsaViz",
                                "values": {
                                    keywordPredicate: [{"text": "T"}],
                                    filterPredicate: [{"text": "T"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Desktop"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)
    
    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Node-centric RDF Graph Visualization",
            "authors": [{"label": "C. Sayers"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "RDF graph visualizer",
                                "values": {
                                    keywordPredicate: [{"text": "T"}],
                                    filterPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Desktop"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "A tool for visualization and editing of OWL ontologies",
            "authors": [{"label": "S. Krivov"}, {"label": "R. Williams"}, {"label": "F. Villa"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "GrOWL",
                                "values": {
                                    keywordPredicate: [{"text": "T"}],
                                    filterPredicate: [{"text": "T"}],
                                    samplingPredicate: [{"text": "T"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Ontology"}],
                                    applicationTypePredicate: [{"label": "Desktop"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "NodeTrix: a Hybrid Visualization of Social Networks",
            "authors": [{"label": "N. Henry"}, {"label": "J. Fekete"}, {"label": "M. J. McGuffin"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "NodeTrix",
                                "values": {
                                    keywordPredicate: [{"text": "F"}],
                                    filterPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "T"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Ontology"}],
                                    applicationTypePredicate: [{"label": "Desktop"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "RDF data exploration and visualization",
            "authors": [{"label": "L. Deligiannidis"}, {"label": "K. Kochut"}, {"label": "A. P. Sheth"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "PGV",
                                "values": {
                                    keywordPredicate: [{"text": "F"}],
                                    filterPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "T"}],
                                    diskPredicate: [{"text": "T"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Desktop"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Browsing Linked Data with Fenfire",
            "authors": [{"label": "T. Hastrup"}, {"label": "R. Cyganiak"}, {"label": "U. Bojars"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "Fenfire",
                                "values": {
                                    keywordPredicate: [{"text": "F"}],
                                    filterPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Desktop"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "An Open Source Software for Exploring and Manipulating Networks",
            "authors": [{"label": "M. Bastian"}, {"label": "S. Heymann"}, {"label": "Jacomy. Gephi"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "Gephi",
                                "values": {
                                    keywordPredicate: [{"text": "F"}],
                                    filterPredicate: [{"text": "T"}],
                                    samplingPredicate: [{"text": "T"}],
                                    aggregationPredicate: [{"text": "T"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Desktop"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Using Clusters in RDF Visualization",
            "authors": [{"label": "J. Dokulil"}, {"label": "J. Katreniaková"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "Trisolda",
                                "values": {
                                    keywordPredicate: [{"text": "F"}],
                                    filterPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "T"}],
                                    aggregationPredicate: [{"text": "T"}],
                                    incrementalPredicate: [{"text": "T"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Desktop"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Visualizing large-scale RDF data using Subsets, Summaries, and Sampling in Oracle",
            "authors": [{"label": "S. Sundara"}, {"label": "M. Atre"}, {"label": "V. Kolovski"} , {"label": "S. Das"} , {"label": "Z. Wu"} , {"label": "E. I. Chong"}, {"label": "J. Srinivasan"}], # 
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "Cytospace",
                                "values": {
                                    keywordPredicate: [{"text": "T"}],
                                    filterPredicate: [{"text": "T"}],
                                    samplingPredicate: [{"text": "T"}],
                                    aggregationPredicate: [{"text": "T"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "T"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Desktop"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "A Visualization Service for the Semantic Web",
            "authors": [{"label": "S. Falconer"}, {"label": "C. Callendar"}, {"label": "M. A. Storey"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "FlexViz",
                                "values": {
                                    keywordPredicate: [{"text": "T"}],
                                    filterPredicate: [{"text": "T"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Interactive Relationship Discovery via the Semantic Web",
            "authors": [{"label": "P. Heim"}, {"label": "S. Lohmann"}, {"label": "T. Stegemann"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "RelFinder",
                                "values": {
                                    keywordPredicate: [{"text": "F"}],
                                    filterPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "ZoomRDF: semantic fisheye zooming on RDF data.",
            "authors": [{"label": "K. Zhang"}, {"label": "H. Wang"}, {"label": "D. T. Tran"}, {"label": "Y. Yu"}], # 
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "ZoomRDF",
                                "values": {
                                    keywordPredicate: [{"text": "F"}],
                                    filterPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "T"}],
                                    aggregationPredicate: [{"text": "T"}],
                                    incrementalPredicate: [{"text": "T"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Desktop"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "A Novel Approach to Visualizing and Navigating Ontologies",
            "authors": [{"label": "E. Motta"}, {"label": "P. Mulholland"}, {"label": "S. Peroni"}, {"label": "M. d’Aquin"}, {"label": "J. M. Gómez-Pérez"}, {"label": "V. Mendez"}, {"label": "F. Zablith"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "KC-Viz",
                                "values": {
                                    keywordPredicate: [{"text": "F"}],
                                    filterPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "T"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Ontology"}],
                                    applicationTypePredicate: [{"label": "Desktop"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "LODWheel - JavaScript-based Visualization of RDF Data.",
            "authors": [{"label": "M. Stuhr"}, {"label": "D. Roman"}, {"label": "D. Norheim"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "LODWheel",
                                "values": {
                                    keywordPredicate: [{"text": "F"}],
                                    filterPredicate: [{"text": "T"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "T"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Using Hierarchical Edge Bundles to visualize complex ontologies in GLOW",
            "authors": [{"label": "W. Hop"}, {"label": "S. de Ridder"}, {"label": "F. Frasincar"}, {"label": "F. Hogenboom"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "GLOW",
                                "values": {
                                    keywordPredicate: [{"text": "F"}],
                                    filterPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "T"}],
                                    aggregationPredicate: [{"text": "T"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Ontology"}],
                                    applicationTypePredicate: [{"label": "Desktop"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "LodLive, exploring the web of data",
            "authors": [{"label": "D. V. Camarda"}, {"label": "S. Mazzini"}, {"label": "A. Antonuccio"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "Lodlive",
                                "values": {
                                    keywordPredicate: [{"text": "T"}],
                                    filterPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Visualizing Populated Ontologies with OntoTrix",
            "authors": [{"label": "B. Bach"}, {"label": "E. Pietriga"}, {"label": "I. Liccardi"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "OntoTrix",
                                "values": {
                                    keywordPredicate: [{"text": "F"}],
                                    filterPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "T"}],
                                    aggregationPredicate: [{"text": "T"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Ontology"}],
                                    applicationTypePredicate: [{"label": "Desktop"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "A Visual Summary for Linked Open Data sources.",
            "authors": [{"label": "F. Benedetti"}, {"label": "L. Po"}, {"label": "S. Bergamaschi"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "LODeX",
                                "values": {
                                    keywordPredicate: [{"text": "F"}],
                                    filterPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "T"}],
                                    aggregationPredicate: [{"text": "T"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "User-Oriented Visualization of Ontologies",
            "authors": [{"label": "S. Lohmann"}, {"label": "S. Negru"}, {"label": "F. Haag"}, {"label": "T. Ertl"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "VOWL 2",
                                "values": {
                                    keywordPredicate: [{"text": "F"}],
                                    filterPredicate: [{"text": "F"}],
                                    samplingPredicate: [{"text": "F"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "F"}],
                                    domainPredicate: [{"text": "Ontology"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "graphVizdb: A Scalable Platform for Interactive Large Graph Visualization.",
            "authors": [{"label": "N. Bikakis"}, {"label": "J. Liagouris"}, {"label": "M. Krommyda"}, {"label": "G. Papastefanatos"}, {"label": "T. Sellis"}], # , {"label": ""}
            "publicationYear": 2003,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": graphVisualizationProblem}
                        ],
                        implementationPredicate: [
                            {
                                "label": "graphVizdb",
                                "values": {
                                    keywordPredicate: [{"text": "T"}],
                                    filterPredicate: [{"text": "T"}],
                                    samplingPredicate: [{"text": "T"}],
                                    aggregationPredicate: [{"text": "F"}],
                                    incrementalPredicate: [{"text": "F"}],
                                    diskPredicate: [{"text": "T"}],
                                    domainPredicate: [{"text": "Generic"}],
                                    applicationTypePredicate: [{"label": "Web"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }
    

    response = orkg.papers.add(paper)

    print(response.content)

if __name__ == "__main__":
    main()
