import json
import argparse
from collections import defaultdict
from orkg import ORKG
from orkg.out import OrkgResponse
from typing import Dict
from tqdm import tqdm
from functools import lru_cache
from typing import Optional


TASK_CLASS = "Problem"

# Global dictionary for paper 
dictionary_papers = {}

# Method lookup tables
real_calls = {}
fake_calls = {}


# Decorator for method selection of real calls to ORKG
def real_call(f):
    return real_calls.setdefault(f.__name__, f)


# Decorator for method selection of fake calls
def fake_call(f):
    return fake_calls.setdefault(f.__name__, f)


# Evil global var for fake ID generation
dummy_counter = 1_000_000


def get_argument():
    parser = argparse.ArgumentParser(description='ORKG Paper Import')

    parser.add_argument('--email', default='paperwithcode@gmail.com', type=str,
                        help="create user for paper with code")
    parser.add_argument('--password', default='paperwithcode', type=str,
                        help="the password for the default user 'paperwithcode@gmail.com'")

    parser.add_argument('--api', default='http://localhost:8080/', type=str,
                        help="The API Path")

    parser.add_argument("--path_evaluation_tables",
                        default='./data/evaluation-tables.json',
                        help="Path to the Evaluation table json from paper with code")

    parser.add_argument("--path_paper_with_abstracts",
                        default='./data/papers-with-abstracts.json',
                        help="Directory with training data.")

    parser.add_argument("--path_title_list",
                        default=None,
                        help="Path to a list of titles that should be imported.")

    parser.add_argument('-d', '--dry-run', dest='dry_run',
                        action='store_true', default=False,
                        help='Simulate calls to ORKG')

    return parser.parse_args()


@lru_cache()
def create_or_find_predicate(label: str):
    """
    This retrieve the ID if already in ORKG but we added the force flag to force the creation of a custom ID
    """
    custom_id = "HAS_" + "_".join(label.upper().split())
    found_id = call['orkg_get_predicate_id'](custom_id)
    if found_id.succeeded:
        return found_id.content['id']       

    found = call['orkg_get_predicate'](label)
    
    if len(found) > 0:
        for item in found:
            if item['id'] == custom_id:
                predicate = item['id']
                return predicate

    predicate = call['orkg_add_predicate'](custom_id, label).content['id']
    return predicate


@real_call
def orkg_get_predicate(label):
    return orkg.predicates.get(q="has " + label, exact=True).content

@real_call
def orkg_get_predicate_id(id):
    return orkg.predicates.by_id(id=id)


@real_call
def orkg_add_predicate(custom_id, label):
    return orkg.predicates.add(id=custom_id, label="has " + label)


@fake_call
def orkg_get_predicate(label):
    return [{'id': "HAS_" + "_".join(label.upper().split())}]

@fake_call
def orkg_get_predicate_id(id):
    # TODO : Need update from python package by Yaser 
    return OrkgResponse(status_code="200", content={'id': "HAS_" + "_".join(label.upper().split())}, url=None, paged=None, response=None)

# noinspection PyUnusedLocal
@fake_call
def orkg_add_predicate(custom_id, label):
    pass


@lru_cache()
def create_or_find_class(label: str):
    custom_id = label.capitalize()
    found_id = call['orkg_get_class_id'](custom_id)
    if found_id.succeeded:
        return found_id.content['id'] 

    found = call['orkg_get_class'](label)
    
    if len(found) > 0:
        for item in found:
            if item['id'] == custom_id:
                clazz = item['id']
                return clazz
    clazz = call['orkg_add_class'](custom_id, label).content['id']
    return clazz


@real_call
def orkg_add_class(custom_id, label):
    return orkg.classes.add(id=custom_id, label=label)


@real_call
def orkg_get_class(label):
    return orkg.classes.get_all(q=label, exact=True).content

@real_call
def orkg_get_class_id(id):
    return orkg.classes.by_id(id=id)

@fake_call
def orkg_get_class(label):
    return [{'id': label.capitalize()}]

@fake_call
def orkg_get_class_id(id):
    pass

# noinspection PyUnusedLocal
@fake_call
def orkg_add_class(custom_id, label):
    pass


@lru_cache(maxsize=None)
def create_or_find_resource(label: str, class_id: str):
    found = call['orkg_get_resource'](class_id, label)

    if len(found) > 0:
        for item in found:
            resource = item['id']
            return resource

    resource = call['orkg_add_resource'](class_id, label).content['id']
    return resource


@real_call
def orkg_add_resource(class_id, label):
    return orkg.resources.add(label=label, classes=[class_id])


@real_call
def orkg_get_resource(class_id, label):
    return orkg.classes.get_resource_by_class(class_id, q=label, exact=True).content


# noinspection PyUnusedLocal
@fake_call
@lru_cache(maxsize=None)
def orkg_get_resource(class_id, label):
    global dummy_counter
    new_id = 'RF' + str(dummy_counter)
    dummy_counter += 1
    print('R:', new_id, "->", label)
    return [{'id': new_id}]


# noinspection PyUnusedLocal
@fake_call
def orkg_add_resource(class_id, label):
    pass


def read_file(path: str) -> Dict:
    """
    Reads the json file and returns a dictionary containing the json object
    
    Parameters
    ----------
    path : str
        The local path of the json file
    """

    file = open(path)
    json_string = json.load(file)
    file.close()
    return json_string


def parse_title_author_publishing(obj: Dict, dictionary: Dict):
    """
    parses the Task json object and add the information into the graph
    
    Note: it might be called recursively
    
    Parameters
    ----------
    obj         : dict
                    the json representation of the Task object
    dictionary  : dict 
                    dictionary that contains maping title- > author, publishing date
    """

    if obj["title"] != "" and obj["title"] is not None:
        title = obj["title"]

        dictionary[title]["url"] = obj["url_pdf"]
        dictionary[title]["publishedIn"] = obj["proceeding"]
        dictionary[title]["authors"] = obj["authors"]
        date = obj["date"].strip()

        if date != "":

            publication_year, publication_month, _ = date.split("-")

            dictionary[title]["publicationYear"] = publication_year
            dictionary[title]["publicationMonth"] = publication_month
        else:
            dictionary[title]["publicationYear"] = ""
            dictionary[title]["publicationMonth"] = ""


def cleanup_paper_title(paper_title: Optional[str]) -> str:
    if paper_title:
        return paper_title.strip()
    return ""  # empty string is truthy, so we can use the value directly


def is_arxiv_paper(paper_url: str) -> bool:
    return paper_url.split("//")[1][:5] == 'arxiv'


def cannot_process(paper_url: str) -> bool:
    # TODO: need to find a clever way to deal with paper not from arxiv
    return paper_url == '' or paper_url[-4:] == 'html' or (not is_arxiv_paper(paper_url))


def update_paper_url(paper_url: str) -> str:
    if paper_url:
        if paper_url[-1] == '/':
            return paper_url[:-1] + '.pdf'
        elif paper_url[-3:] != 'pdf':  # WARNING: This assumes we only process arxiv content! (Which we do right now.)
            return paper_url + '.pdf'
    return paper_url


def simultaneous_parsing_and_writing_into_orkg(task_entry: Dict):
    """
    parses the Task json object and add the information into the graph
    
    Note: it might be called recursively
    
    Parameters
    ----------
    task_entry : dict
        the json representation of the Task object
    """

    datasets = task_entry["datasets"]

    task_name = task_entry["task"]
    task_id = create_or_find_resource(task_name, TASK_CLASS)

    for dt in datasets:
        dataset_name = dt["dataset"]
        dataset_id = create_or_find_resource(dataset_name, dataset_class)
        for _, row in enumerate(dt["sota"]["rows"]):
            # Each row corresponds to one contribution
            title = cleanup_paper_title(row['paper_title'])
            paper_url = update_paper_url(row['paper_url'].strip())

            if not title or cannot_process(paper_url):
                continue
            
            # TODO : can a model name be empty ? 
            model_name = row['model_name']
            model_names = [{'@id': create_or_find_resource(model_name, model_class)}]

            code_links_list = [{'text': link['url']} for link in row['code_links']]

            authors = [{"label": author} for author in dictionary_title_author_publishing[title]["authors"]]
            evaluation_predicate_list = []
            benchmarks_list = []
            for _, (metric, score) in enumerate(row['metrics'].items()):
                metrics_id = create_or_find_resource(metric, metric_class)
                evaluation_predicate_list.append({
                            "label": f"Evaluation {metric}_{score}",
                            "classes": [evaluation_class],
                            "values": {
                                metric_predicate: [
                                    {
                                        "@id": metrics_id
                                    }
                                ],
                                value_predicate: [
                                    {
                                        "text": score
                                    }
                                ]
                            }
                        }) 

            benchmarks_list.append({
                "label": f"Benchmark {dataset_name}",
                "classes": [benchmark_class],
                "values": {
                    dataset_predicate: [
                        {
                            "@id": dataset_id
                        }
                    ],
                    evaluation_predicate: evaluation_predicate_list
                }
            })

            if title not in dictionary_papers:
                paper = {
                    "predicates": [

                    ],
                    "paper": {
                        "title": title,
                        "url": paper_url,
                        "authors": authors,
                        "publicationMonth": dictionary_title_author_publishing[title]["publicationMonth"],
                        "publicationYear": dictionary_title_author_publishing[title]["publicationYear"],
                        "researchField": "R132",
                        "contributions": [
                            create_contribution(benchmarks_list, code_links_list, model_name, model_names, task_id)
                        ]
                    }
                }

                # avoid creation of empty node 
                if len(authors) == 0:
                    del paper["paper"]["authors"]
                if dictionary_title_author_publishing[title]["publicationMonth"].strip() == "":
                    del paper["paper"]["publicationMonth"]
                if dictionary_title_author_publishing[title]["publicationYear"].strip() == "":
                    del paper["paper"]["publicationYear"]

                # Contributions
                if len(code_links_list) == 0:
                    del paper["paper"]["contributions"][0]["values"][source_predicate]
                if len(model_names) == 0:
                    del paper["paper"]["contributions"][0]["values"][model_predicate]
                if len(benchmarks_list) == 0:
                    del paper["paper"]["contributions"][0]["values"][benchmark_predicate]

                dictionary_papers[paper["paper"]["title"]] = paper

            else :
                # in the case we have a already seen paper 
                matcher_model_name = None
                idx_current_model = None
                for idx, contrib in enumerate(dictionary_papers[title]["paper"]["contributions"]):
                    current_model_name = contrib["name"].split("Contribution")[-1].strip()
                    # If one of the benchemarks has the same name with the current candidate
                    if model_name == current_model_name :
                        matcher_model_name = current_model_name
                        idx_current_model = idx
                        break

                if matcher_model_name :
                    # TODO handle the case where we have the same benchmark with different evaluation 
                    find = None
                    idx_benchmk = None
            
                    if not (benchmarks_list[0] in dictionary_papers[title]["paper"]["contributions"][idx_current_model]["values"][benchmark_predicate]):
                        for idx, benchmk in enumerate(dictionary_papers[title]["paper"]["contributions"][idx_current_model]["values"][benchmark_predicate]): 
                            if benchmk["label"] == benchmarks_list[0]["label"]:
                                find = True 
                                idx_benchmk = idx
                                break  

                        if find:
                            find_more_eval = None
                            present_eval_idx = []
                            for idx_eval, evaluation_current in enumerate(benchmarks_list[0]["values"][evaluation_predicate]):
                                for evaluation_saved in dictionary_papers[title]["paper"]["contributions"][idx_current_model]["values"][benchmark_predicate][idx_benchmk]["values"][evaluation_predicate]:
                                    if evaluation_saved["label"] == evaluation_current["label"]:
                                        find_more_eval = True 
                                        present_eval_idx.append(idx_eval)
                                        break 
                                if evaluation_saved["label"] == evaluation_current["label"]:
                                    break
                                

                            if not find_more_eval:
                                dictionary_papers[title]["paper"]["contributions"][idx_current_model]["values"][benchmark_predicate].extend(benchmarks_list[0]["values"][evaluation_predicate])
                            else:
                                # only some part of the runnning evaluation are valid (not duplicate)
                                for idx_eval, evaluation_current in enumerate(benchmarks_list[0]["values"][evaluation_predicate]):
                                    if idx_eval not in present_eval_idx:
                                        dictionary_papers[title]["paper"]["contributions"][idx_current_model]["values"][benchmark_predicate].append(evaluation_current)

                        else:
                            # A different benchmark by name 
                            dictionary_papers[title]["paper"]["contributions"][idx_current_model]["values"][benchmark_predicate].append(benchmarks_list[0])
                        
                else:
                    # If the paper is seen for the first time
                    dictionary_papers[title]["paper"]["contributions"].append(
                                                create_contribution(benchmarks_list, code_links_list, model_name, model_names, task_id))

    subtasks = task_entry["subtasks"]

    for subtask in subtasks:
        simultaneous_parsing_and_writing_into_orkg(subtask)


def create_contribution(benchmarks_list, code_links_list, model_name, model_names, task_id):
    return {
        "name": f"Contribution \t{model_name}",
        "values": {
            source_predicate: code_links_list,
            research_problem_predicate: [
                {
                    "@id": task_id,
                }
            ],
            model_predicate: model_names,
            benchmark_predicate: benchmarks_list
        }
    }


@real_call
def orkg_add_paper(paper):
    orkg.papers.add(params=paper, merge_if_exists=True)


@fake_call
def orkg_add_paper(paper):
    print('PAPER:', json.dumps(paper))
    pass


@real_call
def show_progress(iterable):
    return tqdm(iterable)


@fake_call
def show_progress(iterable):
    return iterable


if __name__ == '__main__':
    args = get_argument()

    evaluation_tables = read_file(args.path_evaluation_tables)
    paper_with_abstracts = read_file(args.path_paper_with_abstracts)

    # Set up filter for specific titles
    title_filter = []
    if args.path_title_list:
        with open(args.path_title_list) as file:
            for line in file.readlines():
                title_filter.append(line.strip())
    if title_filter:
        print("Filtering for", len(title_filter), "titles")

    if not args.dry_run:
        orkg = ORKG(host=args.api, creds=(args.email, args.password))
        call = real_calls
    else:
        orkg = None
        call = fake_calls

    # A dictionary that keep read information from the paper with abstract json to map
    # Title-author-publishing  information for a given paper
    dictionary_title_author_publishing = defaultdict(lambda: defaultdict(lambda: ""))
    print("Processing (papers with abstracts) potion of the data")
    for entry in call['show_progress'](paper_with_abstracts):
        parse_title_author_publishing(entry, dictionary_title_author_publishing)

    # This part is already coded in the python package but we decided to leave it for the moment
    source_predicate = create_or_find_predicate("source code")
    dataset_predicate = create_or_find_predicate("dataset")
    benchmark_predicate = create_or_find_predicate("benchmark")
    model_predicate = create_or_find_predicate("model")
    research_problem_predicate = "P32"  # create_or_find_predicate("Research problem")
    metric_predicate = create_or_find_predicate("METRIC")
    value_predicate = create_or_find_predicate("Value")
    evaluation_predicate = create_or_find_predicate("Evaluation")
    method_predicate = create_or_find_predicate("Method")

    model_class = create_or_find_class("Model")
    dataset_class = create_or_find_class("Dataset")
    metric_class = create_or_find_class("Metric")
    benchmark_class = create_or_find_class("Benchmark")
    evaluation_class = create_or_find_class("Evaluation")

    # Adding Papers
    print("Processing (evaluation tables) potion of the data")
    for entry in call['show_progress'](evaluation_tables):
        simultaneous_parsing_and_writing_into_orkg(entry)

    for paper in call['show_progress'](dictionary_papers):
        paper = dictionary_papers[paper]
        title = paper['paper']['title']
        if title_filter:
            if title in title_filter:
                call['orkg_add_paper'](paper)
        else:
            call['orkg_add_paper'](paper)

    print("Cache Information:")
    print("  Predicates: ", create_or_find_predicate.cache_info())
    print("  Classes:    ", create_or_find_class.cache_info())
    print("  Resources:  ", create_or_find_resource.cache_info())
