from import_pwc_json_to_orkg import cleanup_paper_title


def test_cleaning_blank_paper_title_returns_empty_string():
    assert cleanup_paper_title("  \t") == ''


def test_cleaning_empty_paper_title_returns_empty_string():
    assert cleanup_paper_title('') == ''


def test_cleaning_paper_title_with_surrounding_spaces_returns_trimmed_title():
    assert cleanup_paper_title('  foo  ') == 'foo'


def test_cleaning_paper_title_without_surrounding_spaces_returns_original_title():
    assert cleanup_paper_title('foo') == 'foo'
