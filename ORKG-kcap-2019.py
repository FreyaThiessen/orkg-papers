from orkg import ORKG
from rdflib import Graph, URIRef, Literal, BNode
from rdflib.namespace import RDF, RDFS, XSD
from shortid import ShortId
import pandas as pd
import requests


api = 'http://localhost:8000'
api_resources = '{}/api/resources/'.format(api)
api_predicates = '{}/api/predicates/'.format(api)
api_literals = '{}/api/literals/'.format(api)
api_statements = '{}/api/statements/'.format(api)
api_classes = '{}/api/classes/'.format(api)
sid = ShortId()
orkg = ORKG(host=api)

'''

File to import a paper of the ORKG system

Data from: 
Mohamad Yaser Jaradeh, Allard Oelen, Kheir Eddine Farfar, Manuel Prinz, Jennifer D'Souza, Gábor Kismihók, Markus Stocker, and Sören Auer. 2019.
Open Research Knowledge Graph: Next Generation Infrastructure for Semantic Scholarly Knowledge.
In Proceedings of the 10th International Conference on Knowledge Capture (K-CAP '19). ACM, New York, NY, USA, 243-246. 
DOI: https://doi.org/10.1145/3360901.3364435

'''

# The RDF Data Cube Vocabulary index
cube = dict()
cube['DataSet'] = URIRef('http://purl.org/linked-data/cube#DataSet')
cube['DataStructureDefinition'] = URIRef('http://purl.org/linked-data/cube#DataStructureDefinition')
cube['Observation'] = URIRef('http://purl.org/linked-data/cube#Observation')
cube['ComponentSpecification'] = URIRef('http://purl.org/linked-data/cube#ComponentSpecification')
cube['ComponentProperty'] = URIRef('http://purl.org/linked-data/cube#ComponentProperty')
cube['DimensionProperty'] = URIRef('http://purl.org/linked-data/cube#DimensionProperty')
cube['MeasureProperty'] = URIRef('http://purl.org/linked-data/cube#MeasureProperty')
cube['AttributeProperty'] = URIRef('http://purl.org/linked-data/cube#AttributeProperty')
cube['dataSet'] = URIRef('http://purl.org/linked-data/cube#dataSet')
cube['structure'] = URIRef('http://purl.org/linked-data/cube#structure')
cube['component'] = URIRef('http://purl.org/linked-data/cube#component')
cube['componentProperty'] = URIRef('http://purl.org/linked-data/cube#componentProperty')
cube['componentAttachment'] = URIRef('http://purl.org/linked-data/cube#componentAttachment')
cube['dimension'] = URIRef('http://purl.org/linked-data/cube#dimension')
cube['attribute'] = URIRef('http://purl.org/linked-data/cube#attribute')
cube['measure'] = URIRef('http://purl.org/linked-data/cube#measure')
cube['order'] = URIRef('http://purl.org/linked-data/cube#order')
# Vocabulary Classes
cube_classes = [cube['DataSet'],cube['DataStructureDefinition'],cube['Observation'],cube['ComponentSpecification'],
              cube['ComponentProperty'],cube['DimensionProperty'],cube['MeasureProperty'],cube['AttributeProperty']]
qbDatasetClass = 'QBDataset'


def create_or_find_predicate(label):
    found = orkg.predicates.get(q=label, exact=True).content
    if len(found) > 0:
        predicate = found[0]['id']
    else:
        predicate = orkg.predicates.add(label=label).content['id']
    return predicate


def create_or_find_resource(label):
    found = orkg.resources.get(q=label, exact=True).content
    if len(found) > 0:
        resource = found[0]['id']
    else:
        resource = orkg.resources.add(label=label).content['id']
    return resource


def generate_sid():
    r = sid.generate()
    while r[0].isdigit() or r.startswith(('-', '_')):
        r = sid.generate()
    return r

def get_id(resources, api, r, g):
    if not str(r) in resources or api==api_predicates:
        l = None
        l_classes = []
        if type(r) is Literal:
            l = r
        else:
            l = g.value(r, RDFS.label)
            Ts = [o for s, p , o in g.triples( (r, RDF.type, None))]
            # Set the classes of a ressource
            for T in Ts:
                if(T in cube_classes):
                    lc = 'qb:'+T.split('#')[-1]
                    if lc != 'qb:DataSet': # Beceause we use a fixed ID for qb:DataSet class
                        l_class = requests.get(api_classes, params={'q':lc, 'exact': 'true'}, headers={'Content-Type':'application/json', 'Accept':'application/json'}).json()
                        if len(l_class) == 0:
                            l_classes.append(requests.post(api_classes, json={'label':lc}, headers={'Content-Type':'application/json'}).json()['id'])
                        if len(l_class) == 1:
                            l_classes.append(l_class[0]['id'])
                    else:
                        res = requests.get(api_classes+qbDatasetClass+'/', headers={'Content-Type':'application/json', 'Accept':'application/json'})
                        if res.status_code == 404:
                            l_classes.append(requests.post(api_classes, json={'label':lc, 'id':qbDatasetClass}, headers={'Content-Type':'application/json'}).json()['id'])
                        else:
                            l_classes.append(res.json()['id'])

            # Name a predicate with the ressource ID
            if (api==api_predicates):
                if len(Ts) > 0:
                    if not str(r) in resources:
                        l = requests.post(api_resources, json={'label':l, 'classes':l_classes}, headers={'Content-Type':'application/json'}).json()['id']
                        resources[str(r)] = l
                    else:
                        l = resources[str(r)]

        if l is None:
            raise Exception('Label is none for resource {}'.format(r))
        if (api==api_predicates):
            j = requests.get(api, params={'q':l, 'exact': 'true'}, headers={'Content-Type':'application/json', 'Accept':'application/json'}).json()
            if len(j) == 0:
                return resources, requests.post(api, json={'label':l, 'classes':l_classes}, headers={'Content-Type':'application/json'}).json()['id']
            if len(j) == 1:
                return resources, j[0]['id']
        else:
            resource_id = requests.post(api, json={'label':l, 'classes':l_classes}, headers={'Content-Type':'application/json'}).json()['id']
            resources[str(r)] = resource_id
            return resources, resource_id
    else:
        return resources, resources[str(r)]


# def get_id(resources, api, r, g):
#     if not str(r) in resources or api == api_predicates:
#         l = None
#         l_classes = []
#         if type(r) is Literal:
#             l = r
#         else:
#             l = g.value(r, RDFS.label)
#             Ts = [o for s, p , o in g.triples( (r, RDF.type, None))]
#             # Set the classes of a ressource
#             for T in Ts:
#                 if(T in cube_classes):
#                     lc = 'qb:'+T.split('#')[-1]
#                     if lc != 'qb:DataSet': # Beceause we use a fixed ID for qb:DataSet class
#                         l_class = orkg.classes.get_all(q=lc, exact=True).content
#                         if len(l_class) == 0:
#                             l_classes.append(orkg.classes.add(label=lc).content['id'])
#                         if len(l_class) == 1:
#                             l_classes.append(l_class[0]['id'])
#                     else:
#                         res = orkg.classes.get_all(qbDatasetClass)
#                         if res.status_code == 404:
#                             l_classes.append(orkg.classes.add(label=lc, id=qbDatasetClass).content['id'])
#                         else:
#                             l_classes.append(res.content['id'])
#
#             # Name a predicate with the ressource ID
#             if (api == api_predicates):
#                 if len(Ts) > 0:
#                     if not str(r) in resources:
#                         l = orkg.predicates.add(label=l).content['id']
#                         resources[str(r)] = l
#                     else:
#                         l = resources[str(r)]
#
#         if l is None:
#             raise Exception('Label is none for resource {}'.format(r))
#         if api == api_predicates:
#             j = orkg.predicates.get(q=l, exact=True).content
#             if len(j) == 0:
#                 return resources, orkg.predicates.add(label=l).content['id']
#             if len(j) == 1:
#                 return resources, j[0]['id']
#         else:
#             if api == api_literals:
#                 resource_id = orkg.literals.add(label=l).content['id']
#             else:
#                 resource_id = orkg.resources.add(label=l, classes=l_classes).content['id']
#             resources[str(r)] = resource_id
#             return resources, resource_id
#     else:
#         return resources, resources[str(r)]


def store(g):
    resources = {}
    for s, p, o in g:
        if ((p.n3() in [RDF.type.n3()]) and o in cube_classes) or p.n3() in [RDFS.label.n3()]:
            continue

        resources, s_id = get_id(resources, api_resources, s, g)
        resources, p_id = get_id(resources, api_predicates, p, g)

        if type(o) is Literal:
            cls = 'literal'
            resources, o_id = get_id(resources, api_literals, o, g)
        else:
            cls = 'resource'
            resources, o_id = get_id(resources, api_resources, o, g)
        orkg.statements.add(subject_id=s_id, predicate_id=p_id, object_id=o_id)
    dataset_node = str([s for s, p, o in g.triples((None, RDF.type, cube['DataSet']))][0])
    return resources[dataset_node]


def save_dataset(dataset, title, dimensions):
    gds = Graph()
    # Vocabulary properties labels
    gds.add((RDF.type, RDFS.label, Literal('type')))
    gds.add((RDFS.label, RDFS.label, Literal('label')))
    gds.add((cube['dataSet'], RDFS.label, Literal('dataSet')))
    gds.add((cube['structure'], RDFS.label, Literal('structure')))
    gds.add((cube['component'], RDFS.label, Literal('component')))
    gds.add((cube['componentProperty'], RDFS.label, Literal('component Property')))
    gds.add((cube['componentAttachment'], RDFS.label, Literal('component Attachment')))
    gds.add((cube['dimension'], RDFS.label, Literal('dimension')))
    gds.add((cube['attribute'], RDFS.label, Literal('attribute')))
    gds.add((cube['measure'], RDFS.label, Literal('measure')))
    gds.add((cube['order'], RDFS.label, Literal('order')))
    # Name spaces
    gds.bind('orkg', 'http://orkg.org/vocab/')
    gds.bind('esuc', 'http://orkg.org/vocab/esuc/')
    gds.bind('qb', 'http://purl.org/linked-data/cube#')
    # BNodes
    ds = URIRef('http://orkg.org/vocab/esuc/{}'.format(generate_sid()))  # theDataset
    dsd = URIRef('http://orkg.org/vocab/esuc/{}'.format(generate_sid()))  # theDataStructureDefinition

    '''
    Dataset
    '''
    gds.add((ds, RDF.type, cube['DataSet']))
    gds.add((ds, RDFS.label, Literal(str(title))))
    gds.add((ds, cube['structure'], dsd))
    '''
    DataStructureDefinition
    '''
    gds.add((dsd, RDF.type, cube['DataStructureDefinition']))
    gds.add((dsd, RDFS.label, Literal('Data Structure Definition ESUC')))

    cs = dict()
    dt = dict()
    for index, column in enumerate(dataset.columns, start=1):
        cs[column] = URIRef('http://orkg.org/vocab/esuc/{}'.format(generate_sid()))
        dt[column] = URIRef('http://orkg.org/vocab/esuc/{}'.format(generate_sid()))
        gds.add((dsd, cube['component'], cs[column]))

        gds.add((cs[column], RDF.type, cube['ComponentSpecification']))
        gds.add((cs[column], RDFS.label, Literal('Component Specification ' + column)))
        gds.add((cs[column], cube['order'], Literal(index)))

        if column in dimensions:
            gds.add((cs[column], cube['dimension'], dt[column]))
            gds.add((dt[column], RDF.type, cube['DimensionProperty']))
        else:
            gds.add((cs[column], cube['measure'], dt[column]))
            gds.add((dt[column], RDF.type, cube['MeasureProperty']))
        gds.add((dt[column], RDF.type, cube['ComponentProperty']))
        gds.add((dt[column], RDFS.label, Literal(column)))

    for index, row in dataset.iterrows():
        bno = URIRef('http://orkg.org/vocab/esuc/{}'.format(generate_sid()))
        gds.add((bno, RDF.type, cube['Observation']))
        gds.add((bno, RDFS.label, Literal('Observation #{}'.format(index + 1))))
        gds.add((bno, cube['dataSet'], ds))
        for column in dataset.columns:
            gds.add((bno, dt[column], Literal(str(row[column]))))
    dataset_resource_id = store(gds)
    return dataset_resource_id


def main():
    ############################# PROPERTIES AND RESOURCES #############################
    impl_predicate = create_or_find_predicate("Implementation")
    eval_predicate = create_or_find_predicate("Evaluation")
    use_framework_predicate = create_or_find_predicate("Uses framework")
    use_library_predicate = create_or_find_predicate("Uses library")
    supports_rdf_predicate = create_or_find_predicate("Supports RDF")
    programming_lang_predicate = create_or_find_predicate("Programming language")
    uses_graph_store_predicate = create_or_find_predicate("Uses graph store")
    description_predicate = create_or_find_predicate("Description")
    url_predicate = create_or_find_predicate("Url")
    utilizes_predicate = create_or_find_predicate("Utilizes")
    performed_at_predicate = create_or_find_predicate("Performed at")
    cohort_size_predicate = create_or_find_predicate("Cohort size")
    yields_predicate = create_or_find_predicate("Yields")
    on_predicate = create_or_find_predicate("On")
    has_formula_predicate = create_or_find_predicate("Has formula")
    demo_video_predicate = create_or_find_predicate("Demo video")
    arch_predicate = create_or_find_predicate("Architecture")
    has_part_predicate = create_or_find_predicate("Has Part")
    figure_predicate = create_or_find_predicate("Figure")

    problem_1 = create_or_find_resource(label='Scholarly communications representation')
    problem_2 = create_or_find_resource(label='Structured descriptions of research contributions')
    problem_3 = create_or_find_resource(label='User interaction')
    problem_4 = create_or_find_resource(label='Similarity measures')
    neo4j = create_or_find_resource(label='Neo4j')
    virtuoso = create_or_find_resource(label='Virtuoso triple store')
    spring_boot = create_or_find_resource(label='Spring Boot')
    spring_security = create_or_find_resource(label='Spring Security')
    kotlin = create_or_find_resource(label='Kotlin')
    java = create_or_find_resource(label='Java')
    python = create_or_find_resource(label='Python')
    js = create_or_find_resource(label='Javascript')
    flask = create_or_find_resource(label='Flask')

    eval_df = pd.read_csv('./data/Dils2018-eval-orkg.csv')
    qb_ds = save_dataset(eval_df, 'DILS2018 User evaluation of ORKG frontend', ['Participant Nr'])

    eval_df2 = pd.read_csv('./data/Dils2018-eval-orkg-sim.csv')
    qb_ds2 = save_dataset(eval_df2, 'DILS2018 technical evaluation of ORKG comparison service', ['System'])

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Open Research Knowledge Graph: Next Generation Infrastructure for Semantic Scholarly Knowledge",
            "authors": [{"label": "Mohamad Yaser Jaradeh", "orcid": "0000-0001-8777-2780"},
                        {"label": "Allard Oelen", "orcid": "0000-0001-9924-9153"},
                        {"label": "Kheir Eddine Farfar", "orcid": "0000-0002-0366-4596"},
                        {"label": "Manuel Prinz", "orcid": "0000-0003-2151-4556"},
                        {"label": "Jennifer D’Souza", "orcid": "0000-0002-6616-9509"},
                        {"label": "Gábor Kismihók", "orcid": "0000-0003-3758-5455"},
                        {"label": "Markus Stocker", "orcid": "0000-0001-5492-3212"},
                        {"label": "Sören Auer", "orcid": "0000-0002-0698-2864"}],
            "doi": "10.1145/3360901.3364435",
            "publicationYear": 2019,
            "publicationMonth": 11,
            "researchField": "R135",
            "contributions": [
                {
                    "name": "Literature Comparison",
                    "values": {
                        "P32": [
                            {"@id": problem_1},
                            {"@id": problem_2}
                        ],
                        has_formula_predicate: [
                            {"text": """$$\\varphi_{i,j} =(\\Phi_{i,j})_{\\substack{c_i \\in \\mathcal{C}\\\\ p_j \\in sim(p) }}, \\Phi_{i,j} = \\begin{cases} 1 & \\text{ if } p_{j} \\in c_i \\\\  0 & \\text{ otherwise }   \\end{cases}$$"""}
                        ],
                        impl_predicate: [
                            {"@id": "_temp_simcomp_1"}
                        ],
                        demo_video_predicate: [
                            {"text": "https://www.youtube.com/watch?v=mbe-cVyW_us"}
                        ],
                        utilizes_predicate: [
                            {
                                "label": "FastText embedding",
                                "values": {
                                    description_predicate: [
                                        {
                                            "text": "Multilingual word embeddings created by Facebook"
                                        }
                                    ],
                                    url_predicate: [
                                        {"text": "https://fasttext.cc/"}
                                    ]
                                }
                            },
                        ],
                        eval_predicate: [
                            {
                                "label": "DILS2018 technical evaluation",
                                "values": {
                                    description_predicate: [
                                        {
                                            "text": "Benchmarks the comparison system against a naive baseline in terms of time"
                                        }
                                    ],
                                    on_predicate: [
                                        {"@id": "_temp_simcomp_1"}
                                    ],
                                    yields_predicate: [
                                        {"@id": qb_ds2}
                                    ]
                                }
                            }
                        ],
                        description_predicate: [
                            {"text": "Conceptional background of the state-of-the-art comparison feature"}
                        ]
                    }
                },
                {
                    "name": "Contribution similarity",
                    "values": {
                        "P32": [
                            {"@id": problem_4},
                            {"@id": problem_2}
                        ],
                        utilizes_predicate: [
                            {"@id": "_temp_ES_1"},
                            {
                                "label": "TF/iDF",
                                "values": {
                                    description_predicate: [
                                        {"text": "Term frequency, inverse document frequency technique"}
                                    ]
                                }
                            }
                        ],
                        impl_predicate: [
                            {"@id": "_temp_simcomp_1"}
                        ],
                        description_predicate: [
                            {"text": "Find similar research contributions inside the ORKG and suggest them to the user"}
                        ]
                    }
                },
                {
                    "name": " ORKG System",
                    "values": {
                        "P32": [
                            {"@id": problem_3},
                            {"@id": problem_2}
                        ],
                        arch_predicate: [
                            {
                                "label": "Classical layered architecture",
                                "values": {
                                    has_part_predicate: [
                                        {"text": "Services layer"},
                                        {"text": "Restful API layer"},
                                        {"text": "Querying layer"},
                                        {"text": "RDF interoperability layer"},
                                        {"text": "Domain model layer"},
                                        {"text": "Versioning and provenance layer"},
                                        {"text": "Persistence layer"}
                                    ],
                                    figure_predicate: [
                                        {"text": "https://doi.org/10.6084/m9.figshare.11395362.v1"}
                                    ]
                                }
                            }
                        ],
                        impl_predicate: [
                            {
                                "@temp": "_temp_frontend_1",
                                "label": "Frontend",
                                "values": {
                                    description_predicate: [
                                        {
                                            "text": "The user interface, which contains the main three functionalities of curation, contribution, and Exploration."
                                        }
                                    ],
                                    use_library_predicate: [
                                        {
                                            "label": "React JS",
                                            "values": {
                                                url_predicate: [
                                                    {"text": "https://reactjs.org/"}
                                                ]
                                            }
                                        }
                                    ],
                                    programming_lang_predicate: [
                                        {"@id": js}
                                    ],
                                    url_predicate: [
                                        {"text": "https://gitlab.com/TIBHannover/orkg/orkg-frontend"}
                                    ]
                                }
                            },
                            {
                                "label": "Backend",
                                "values": {
                                    description_predicate: [
                                        {
                                            "text": "Where the data is stored and processed, It contains the business logic and the Rest API code"
                                        }
                                    ],
                                    use_framework_predicate: [
                                        {"@id": spring_boot},
                                        {"@id": spring_security}
                                    ],
                                    programming_lang_predicate: [
                                        {"@id": kotlin},
                                        {"@id": java}
                                    ],
                                    uses_graph_store_predicate: [
                                        {"@id": neo4j},
                                        {"@id": virtuoso}
                                    ],
                                    url_predicate: [
                                        {"text": "https://gitlab.com/TIBHannover/orkg/orkg-backend"}
                                    ]
                                }
                            },
                            {
                                "@temp": "_temp_simcomp_1",
                                "label": "Simcomp",
                                "values": {
                                    description_predicate: [
                                        {
                                            "text": "Similarity and Comparision framework, to compare research contributions and find simialiatiy among them"}
                                    ],
                                    use_framework_predicate: [
                                        {"@id": flask}
                                    ],
                                    use_library_predicate: [
                                        {"label": "numpy"},
                                        {"label": "pandas"}
                                    ],
                                    utilizes_predicate: [
                                        {
                                            "@temp": "_temp_ES_1",
                                            "label": "ES",
                                            "values": {
                                                description_predicate: [
                                                    {
                                                        "text": "Elastic search indexing engine"
                                                    }
                                                ],
                                                url_predicate: [
                                                    {"text": "https://www.elastic.co/"}
                                                ]
                                            }
                                        },
                                    ],
                                    programming_lang_predicate: [
                                        {"@id": python}
                                    ],
                                    url_predicate: [
                                        {"text": "https://gitlab.com/TIBHannover/orkg/orkg-similarity"}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "DILS2018 user evaluation",
                                "values": {
                                    description_predicate: [
                                        {
                                            "text": "Aimed to determine user performance, identify major (positive and negative) aspects, and useracceptance/perception of the system"
                                        }
                                    ],
                                    performed_at_predicate: [
                                        {"text": "DILS2018 Conference"}
                                    ],
                                    cohort_size_predicate: [
                                        {"text": 12}
                                    ],
                                    yields_predicate: [
                                        {"@id": qb_ds}
                                    ],
                                    on_predicate: [
                                        {"@id": "_temp_frontend_1"}
                                    ]                                }
                            }
                        ],
                        supports_rdf_predicate: [
                            {"text": "True"}
                        ],
                        url_predicate: [
                            {"text": "http://orkg.org/"}
                        ]
                    }
                },
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)


if __name__ == "__main__":
    main()
