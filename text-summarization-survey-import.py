import requests
from orkg import ORKG

orkg = ORKG()
vocab = dict()

'''

File to import a survey paper that nicely demonstrates the possibilities of the ORKG comparison feature

Data from: 
Bharti, S.K., Babu, K.S., & Pradhan, A. (2017). Automatic Keyword Extraction for Text Summarization in Multi-document e-Newspapers Articles.
https://arxiv.org/abs/1704.03242

'''


def create_or_find_predicate(label):
    found = orkg.predicates.get(q=label, exact=True).content
    if len(found) > 0:
        predicate = found[0]['id']
    else:
        predicate = orkg.predicates.add(label=label).content['id']
    return predicate


def create_or_find_resource(label):
    found = orkg.resources.get(q=label, exact=True).content
    if len(found) > 0:
        resource = found[0]['id']
    else:
        resource = orkg.resources.add(label=label).content['id']
    return resource


def main():
    ############################# PROPERTIES AND RESOURCES #############################
    summarizationProblem = orkg.resources.add(label='Automatic text summarization').content['id']

    toa_predicate = create_or_find_predicate(label='Approach type')
    dt_predicate = create_or_find_predicate(label='Document type')
    tosu_predicate = create_or_find_predicate(label='Summary usage')
    cos_predicate = create_or_find_predicate(label='Summary characteristics')
    metric_predicate = create_or_find_predicate(label='Metric')
    eval_predicate = create_or_find_predicate("Evaluation")
    impl_predicate = create_or_find_predicate("Implementation")

    toa_a1 = orkg.resources.add(label='Statistical').content['id']
    toa_a2 = orkg.resources.add(label='Machine learning').content['id']
    toa_a3 = orkg.resources.add(label='Coherent Based').content['id']
    toa_a4 = orkg.resources.add(label='Graph based').content['id']
    toa_a5 = orkg.resources.add(label='Algebraic').content['id']

    dt_d1 = orkg.resources.add(label='Single document').content['id']
    dt_d2 = orkg.resources.add(label='Multiple documents').content['id']

    tosu_s1 = orkg.literals.add(label='Generic').content['id']
    tosu_s2 = orkg.literals.add(label='Query based').content['id']

    cos_c1 = orkg.literals.add(label='Extractive').content['id']
    cos_c2 = orkg.literals.add(label='Abstractive').content['id']

    metric_m1_r1 = orkg.resources.add(label='ROUGE 1').content['id']
    metric_m1_r2 = orkg.resources.add(label='ROUGE 2').content['id']
    metric_m1_rl = orkg.resources.add(label='ROUGE L').content['id']
    metric_m1_rw = orkg.resources.add(label='ROUGE W').content['id']
    metric_m1_rs = orkg.resources.add(label='ROUGE SU4').content['id']
    metric_m2_p = create_or_find_resource(label='Precision')
    metric_m2_r = orkg.resources.add(label='Recall').content['id']
    metric_m2_f1 = orkg.resources.add(label='F-measure').content['id']
    metric_m2_ru = orkg.resources.add(label='Relative utility').content['id']

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Automatic Abstracting Research at Chemical Abstracts Service",
            "authors": [{"label": "Joseph J. Pollock"}, {"label": "Antonio Zamora"}],
            "doi": "10.1021/ci60004a008",
            "publicationYear": 1975,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d1}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c2}
                                    ]
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Automatic condensation of electronic publications by sentence selection",
            "authors": [{"label": "Ronald Brandow"}, {"label": "Karl Mitze"}, {"label": "Lisa F.Rau"}],
            "doi": "10.1016/0306-4573(95)00052-I",
            "publicationMonth": 9,
            "publicationYear": 1995,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c2}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Automated text summarization and the SUMMARIST system",
            "authors": [{"label": "Eduard Hovy"}, {"label": "Chin-Yew Lin"}],
            "doi": "10.3115/1119089.1119121",
            "publicationYear": 1998,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "SUMMARIST",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    tosu_predicate: [
                                        {"@id": tosu_s1}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Trainable, scalable summarization using robust NLP and machine learning",
            "authors": [{"label": "Chinatsu Aone"}, {"label": "Mary Ellen Okurowski"}, {"label": "James Gorlinsky"}],
            "doi": "10.3115/980845.980856",
            "publicationYear": 1998,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d1}
                                    ],
                                    tosu_predicate: [
                                        {"@id": tosu_s1}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Generating Natural Language Summaries from Multiple On-Line Sources",
            "authors": [{"label": "Dragomir R. Radev"}, {"label": "Kathleen R. McKeown"}],
            "publicationYear": 1998,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "SUMMONS",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1},
                                        {"@id": cos_c2}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Discourse Trees Are Good Indicators of Importance in Text",
            "authors": [{"label": "Daniel Marcu"}],
            "publicationYear": 1999,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d1}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Information Fusion in the Context of Multi-Document Summarization",
            "authors": [{"label": "Regina Barzilay"}, {"label": "Kathleen R. McKeown"}, {"label": "Michael Elhadad"}],
            "publicationYear": 1999,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c2}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "A multilingual news summarizer",
            "authors": [{"label": "Hsin-Hsi Chen"}, {"label": "Chuan-Jie Lin"}],
            "doi": "10.3115/990820.990844",
            "publicationYear": 2000,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Multilingual Sunmmrization System",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a4}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "Experiments in Single and Multi-Document Summarization Using MEAD",
            "authors": [{"label": "Dragomir R. Radev"}, {"label": "Sasha Blair Goldensohn"}, {"label": "Zhu Zhang"}],
            "publicationYear": 2001,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "NewsInEssence: A System For Domain-Independent, Real-Time News Clustering and Multi-Document Summarization",
            "authors": [{"label": "Dragomir R. Radev"}, {"label": "Sasha Blair Goldensohn"}, {"label": "Zhu Zhang"}, {"label": "Revathi Sundara Raghavan"}],
            "publicationYear": 2001,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "NewsInEssence",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a4}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################

    paper = {
        "paper": {
            "title": "WebInEssence: A Personalized Web-Based Multi-Document Summarization and Recommendation System",
            "authors": [{"label": "Dragomir R. Radev"}, {"label": "Sasha Blair Goldensohn"}, {"label": "Zhu Zhang"}],
            "publicationYear": 2001,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "WebInEssence",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    tosu_predicate: [
                                        {"@id": tosu_s1}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Lin et al. [59]

    paper = {
        "paper": {
            "title": "Automated multi-document summarization in NeATS",
            "authors": [{"label": "Chin-Yew Lin"}, {"label": "Eduard Hovy"}],
            "publicationYear": 2002,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "NeATS",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a4}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## McKeown et al. [60]

    paper = {
        "paper": {
            "title": " Tracking and summarizing news on a daily basis with Columbia's Newsblaster",
            "authors": [{"label": "Kathleen R. McKeown"}, {"label": "Regina Barzilay"}, {"label": "David Evans"},
                        {"label": "Vasileios Hatzivassiloglou"}, {"label": "Judith L. Klavans"}, {"label": "Ani Nenkova"},
                        {"label": "Carl Sable"}, {"label": "Barry Schiffman"}, {"label": "Sergey Sigelman"}],
            "publicationYear": 2002,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Columbia's Newsblaster",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a4}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Daume et al. [58]

    paper = {
        "paper": {
            "title": "GLEANS: A Generator of Logical Extracts and Abstracts for Nice Summaries",
            "authors": [{"label": "Hal Daume"}, {"label": "Abdessamad Echihabi"}, {"label": "Daniel Marcu"},
                        {"label": "Dragos Stefan Munteanu"}, {"label": "Radu Soricut"}],
            "publicationYear": 2002,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "GLEANS",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1},
                                        {"@id": cos_c2}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Harabagiu et al. [36]

    paper = {
        "paper": {
            "title": "Generating Single and Multi-Document Summaries with GISTEXTER",
            "authors": [{"label": "Sanda M. Harabagiu"}, {"label": "Finley Lacatusu"}],
            "publicationYear": 2002,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "GISTEXTER",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d1},
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1},
                                        {"@id": cos_c2}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Saggion et al. [61]

    paper = {
        "paper": {
            "title": "Generating Indicative-Informative Summaries with SumUM",
            "authors": [{"label": "Horacio Saggion"}, {"label": "Guy Lapalme"}],
            "publicationYear": 2002,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "SumUM",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c2}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Saggion et al. [37]

    paper = {
        "paper": {
            "title": "Robust Generic and Query-based Summarization",
            "authors": [{"label": "Horacio Saggion"}, {"label": "Kalina Bontcheva"}, {"label": "Hamish Cunningham"}],
            "publicationYear": 2003,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d1}
                                    ],
                                    tosu_predicate: [
                                        {"@id": tosu_s1},
                                        {"@id": tosu_s2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Chali et al.[62]

    paper = {
        "paper": {
            "title": "The University of Lethbridge Text Summarizer at DUC 2002",
            "authors": [{"label": "Meru Brunn"}, {"label": "Yllias Chali"}, {"label": "Barbara Dufour"}],
            "publicationYear": 2003,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Lethbridge Text Summarizer",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a4}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d1},
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Alfonseca et al. [64]

    paper = {
        "paper": {
            "title": "Description of the uam system for generating very short summaries at DUC–2003",
            "authors": [{"label": "Enrique Alfonseca"}, {"label": "Jośe Maria Guira"}, {"label": "Antonio Moreno-Sandoval"}],
            "publicationYear": 2003,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d1}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "DUC 2003 evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Erkan et al. [65]

    paper = {
        "paper": {
            "title": "The University of Michigan at DUC 2004",
            "authors": [{"label": "Günes Erkan"}, {"label": "Dragomir R. Radev"}],
            "publicationYear": 2004,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "MEAD",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a4}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "DUC 2004 evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Filatova et al.[10]

    paper = {
        "paper": {
            "title": "Event-Based Extractive Summarization",
            "authors": [{"label": "Elena Filatova"}, {"label": "Vasileios Hatzivassiloglou"}],
            "publicationYear": 2004,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "MEAD",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "DUC 2004 evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Nobata et al.[66]

    paper = {
        "paper": {
            "title": "CRL/NYU Summarization System at DUC-2004",
            "authors": [{"label": "Chikashi Nobata"}, {"label": "Satoshi Sekine"}],
            "publicationYear": 2004,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "CRL/NYU",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "CRL/NYU evaluation at DUC 2004",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Conroy et al.[11]

    paper = {
        "paper": {
            "title": "CLASSY Query-Based Multi-Document Summarization",
            "authors": [{"label": "John M. Conroy"}, {"label": "Judith D. Schlesinger"}],
            "publicationYear": 2005,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "CLASSY",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    tosu_predicate: [
                                        {"@id": tosu_s2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Farzindar et al. [48]

    paper = {
        "paper": {
            "title": "CATS a topic-oriented multi-document summarization system at DUC 2005",
            "authors": [{"label": "Atefeh Farzindar"}, {"label": "Frederik Rozon"}, {"label": "Guy Lapalme"}],
            "publicationYear": 2005,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "CATS",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    tosu_predicate: [
                                        {"@id": tosu_s2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation at DUC 2005",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Witte et al.[67]

    paper = {
        "paper": {
            "title": "ERSS 2005: Coreference-Based Summarization Reloaded",
            "authors": [{"label": "René Witte"}, {"label": "Ralf Krestel"}, {"label": "Sabine Bergler"}],
            "publicationYear": 2005,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "ERSS 2005",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Witte et al.[68]

    paper = {
        "paper": {
            "title": "Context-based Multi-Document Summarization using Fuzzy Coreference Cluster Graphs",
            "authors": [{"label": "René Witte"}, {"label": "Ralf Krestel"}],
            "publicationYear": 2006,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "ERSS summarizer",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a4}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## He et al.[69]

    paper = {
        "paper": {
            "title": "MSBGA: A Multi-Document Summarization System Based on Genetic Algorithm",
            "authors": [{"label": "Yan-xiang He"}, {"label": "De-xi Liu"},
                        {"label": "Dong-hong Ji"}, {"label": "Hua Yang"},
                        {"label": "Chong Teng"}],
            "doi":"10.1109/ICMLC.2006.258921",
            "publicationYear": 2006,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "MSBGA",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)


    ############################# PAPER #############################
    ## Witte et al.[13]

    paper = {
        "paper": {
            "title": "Fuzzy Clustering for Topic Analysis and Summarization of Document Collections",
            "authors": [{"label": "René Witte"}, {"label": "Sabine Bergler"}],
            "publicationYear": 2007,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a4}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Fuentes et al.[49]

    paper = {
        "paper": {
            "title": "FEMsum at DUC 2007",
            "authors": [{"label": "Maria Fuentes Fort"},
                        {"label": "Horacio Rodríguez", "orcid": "http://orcid.org/0000-0002-5314-6631"},
                        {"label": "Daniel Ferrés"}],
            "publicationYear": 2007,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "FEMsum",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d1},
                                        {"@id": dt_d2}
                                    ],
                                    tosu_predicate: [
                                        {"@id": tosu_s2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Dunlavy et al.[70]

    paper = {
        "paper": {
            "title": "QCS: A system for querying, clustering and summarizing documents",
            "authors": [{"label": "Daniel M. Dunlavy"}, {"label": "Dianne P. O'leary"},
                        {"label": "John M Conroy"}, {"label": "Judith D. Schlesinger"}],
            "doi": "10.1016/j.ipm.2007.01.003",
            "publicationYear": 2007,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "QCS",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d1},
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Gotti et al.[71]

    paper = {
        "paper": {
            "title": "GOFAIsum: a symbolic summarizer for DUC",
            "authors": [{"label": "Fabrizio Gotti"}, {"label": "Guy Lapalme"},
                        {"label": "Luka Nerima"}, {"label": "Eric Wehrli"}],
            "publicationYear": 2007,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "GOFAIsum",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "DUC Evaluation 2207",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Svore et al.[72]

    paper = {
        "paper": {
            "title": "Enhancing Single-Document Summarization by Combining RankNet and Third-Party Sources",
            "authors": [{"label": "Krysta Svore"}, {"label": "Lucy Vanderwende"},
                        {"label": "Christopher Burges"}],
            "publicationYear": 2007,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "NetSum",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Schilder et al.[12]

    paper = {
        "paper": {
            "title": "FastSum: fast and accurate query-based multi-document summarization",
            "authors": [{"label": "Frank Schilder"}, {"label": "Ravikumar Kondadadi"}],
            "publicationYear": 2008,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "FastSum",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Liu et al.[73]

    paper = {
        "paper": {
            "title": "Personalized PageRank Based Multi-document Summarization",
            "authors": [{"label": "Yong Liu"}, {"label": "Xiaolei Wang"},
                        {"label": "Jin Zhang"}, {"label": "Hongbo Xu"}],
            "doi": "10.1109/WSCS.2008.32",
            "publicationYear": 2008,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "PPRSum",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Zhang et al.[74]

    paper = {
        "paper": {
            "title": "AdaSum: an adaptive model for summarization",
            "authors": [{"label": "Jin Zhang"}, {"label": "Xueqi Cheng"},
                        {"label": "Gaowei Wu"}, {"label": "Hongbo Xu"}],
            "doi": "10.1145/1458082.1458201",
            "publicationYear": 2008,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "AdaSum",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Aramaki et al.[75]

    paper = {
        "paper": {
            "title": "TEXT2TABLE: Medical Text Summarization System Based on Named Entity Recognition and Modality Identification",
            "authors": [{"label": "Eiji Aramaki"}, {"label": "Yasuhide Miura"}, {"label": "Masatsugu Tonoike"},
                        {"label": "Tomoko Ohkuma"}, {"label": "Hiroshi Mashuichi"}, {"label": "Kazuhiko Ohe"}],
            "publicationYear": 2009,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "TEXT2TABLE",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Fisher et al.[50]

    paper = {
        "paper": {
            "title": "OHSU Summarization and Entity Linking Systems",
            "authors": [{"label": "Seeger Fisher"}, {"label": "Aaron Dunlop"},
                        {"label": "Brian Roark"}, {"label": "Yongshun Chen"}, {"label": "Joshua Burmeister"}],
            "publicationYear": 2009,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    tosu_predicate: [
                                        {"@id": tosu_s2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)
    
    print(response.content)

    ############################# PAPER #############################
    ## Hachey et al.[47]

    paper = {
        "paper": {
            "title": "Multi-document summarisation using generic relation extraction",
            "authors": [{"label": "Ben Hachey"}],
            "publicationYear": 2009,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    tosu_predicate: [
                                        {"@id": tosu_s1}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Wei et al.[76]

    paper = {
        "paper": {
            "title": "TIARA: A Visual Exploratory Text Analytic System",
            "authors": [{"label": "Furu Wei"}, {"label": "Shixia Liu"}, {"label": "Yangqiu Song"},
                        {"label": "Shimei Pan"}, {"label": "Michelle X. Zhou"}, {"label": "Weihong Qian"},
                        {"label": "Lei Shi"}, {"label": "Li Tan"}, {"label": "Qiang Zhang"}],
            "doi": "10.1145/1835804.1835827",
            "publicationYear": 2010,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "TIARA",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)
    
    print(response.content)

    ############################# PAPER #############################
    ## Dong et al.[51]

    paper = {
        "paper": {
            "title": "Towards recency ranking in web search",
            "authors": [{"label": "Anlei Dong"}, {"label": "Yi Chang"}, {"label": "Zhaohui Zheng"},
                        {"label": "Gilad Mishne"}, {"label": "Jing Bai"}, {"label": "Ruiqiang Zhang"},
                        {"label": "Karolina Buchner"}, {"label": "Ciya Liao"}, {"label": "Fernando Diaz"}],
            "doi": "10.1145/1718487.1718490",
            "publicationYear": 2010,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    tosu_predicate: [
                                        {"@id": tosu_s2}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Shi et al.[38]

    paper = {
        "paper": {
            "title": "Understanding Text Corpora with Multiple Facets",
            "authors": [{"label": "Lei Shi"}, {"label": "Furu Wei"}, {"label": "Shixia Liu"},
                        {"label": "Li Tan"}, {"label": "Xiaoxiao Lian"}, {"label": "Michelle X. Zhou"}],
            "doi": "10.1109/vast.2010.5652931",
            "publicationYear": 2010,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Park et al.[87]

    paper = {
        "paper": {
            "title": "Automatic Multi-document Summarization Based on Clustering and Nonnegative Matrix Factorization",
            "authors": [{"label": "Sun Park"}, {"label": "ByungRea Cha"}, {"label": "Dong Un An"}],
            "doi": "10.4103/0256-4602.60169",
            "publicationYear": 2010,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a4}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    tosu_predicate: [
                                        {"@id": tosu_s2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Archambault et al.[14]

    paper = {
        "paper": {
            "title": "ThemeCrowds: multiresolution summaries of twitter usage",
            "authors": [{"label": "Daniel Archambault"}, {"label": "Derek Greene"}, {"label": "Pádraig Cunningham"},
                        {"label": "Neil Hurley"}],
            "doi": "10.1145/2065023.2065041",
            "publicationYear": 2011,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "ThemeCrowds",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Genest et al.[41]

    paper = {
        "paper": {
            "title": "Framework for Abstractive Summarization using Text-to-Text Generation",
            "authors": [{"label": "Pierre-Etienne Genest"}, {"label": "Guy Lapalme"}],
            "publicationYear": 2011,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Tsarev et al.[42]

    paper = {
        "paper": {
            "title": "Using NMF-based text summarization to improve supervised and unsupervised classification",
            "authors": [{"label": "Dmitry-V.-Tsarev"}, {"label": "Mikhail Petrovskiy"}, {"label": "Igor Mashechkin"}],
            "doi": "10.1109/HIS.2011.6122102",
            "publicationYear": 2011,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1},
                                        {"@id": toa_a4}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Alguliev et al.[39]

    paper = {
        "paper": {
            "title": "MCMR: Maximum coverage and minimum redundant text summarization model",
            "authors": [{"label": "Rasim Alguliyev"}, {"label": "Ramiz Aliguliyev"},
                        {"label": "Makrufa S. Hajirahimova"}, {"label": "Chingiz A. Mehdiyev"}],
            "doi": "10.1016/j.eswa.2011.05.033",
            "publicationYear": 2011,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "MCMR",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a4}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d1},
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    ## Chandra et al.[90]

    paper = {
        "paper": {
            "title": "A Statistical Approach for Automatic Text Summarization by Extraction",
            "authors": [{"label": "Munehs Chandra"}, {"label": "Vikrant Gupta"}, {"label": "Santosh Kr. Paul"}],
            "publicationYear": 2011,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "KSRS",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)
    
    print(response.content)

    ############################# PAPER #############################
    ## Mirroshandel et al.[44]

    paper = {
        "paper": {
            "title": "Towards Unsupervised Learning of Temporal Relations between Events",
            "authors": [{"label": "Seyed Abolghasem Mirroshandel"}, {"label": "Gholamreza Ghassem-Sani"}],
            "doi": "10.1613/jair.3693",
            "publicationYear": 2012,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1},
                                        {"@id": toa_a4}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)
    
    print(response.content)

    ############################# PAPER #############################
    ## Min et al.[40]

    paper = {
        "paper": {
            "title": "Exploiting Category-Specific Information for Multi-Document Summarization",
            "authors": [{"label": "Jun-Ping Ng"}, {"label": "Praveen Bysani"}, {"label": "Ziheng Lin"},
                        {"label": "Min-Yen Kan"}, {"label": "Chew-Lim Tan"}],
            "publicationYear": 2012,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "SWING",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    tosu_predicate: [
                                        {"@id": tosu_s1}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m1_r1},
                                        {"@id": metric_m1_r2},
                                        {"@id": metric_m1_rl},
                                        {"@id": metric_m1_rw},
                                        {"@id": metric_m1_rs}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)
    
    print(response.content)

    ############################# PAPER #############################
    ## De Melo et al.[43]

    paper = {
        "paper": {
            "title": "UWN: A Large Multilingual Lexical Knowledge Base",
            "authors": [{"label": "Gerard de Melo"}, {"label": "Gerhard Weikum"}],
            "publicationYear": 2012,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "UWN",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a5}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d2}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)
    
    print(response.content)

    ############################# PAPER #############################
    ## Tomas et al.[5]

    paper = {
        "paper": {
            "title": "Automatic Keyword Extraction for Text Summarization in e-Newspapers",
            "authors": [{"label": "Justine Raju Thomas"}, {"label": "Santosh Kumar Bharti"}, {"label": "Korra Sathya Babu"}],
            "doi": "10.1145/2980258.2980442",
            "publicationYear": 2012,
            "researchField": "R133",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": summarizationProblem}
                        ],
                        impl_predicate: [
                            {
                                "label": "Implementation",
                                "values": {
                                    toa_predicate: [
                                        {"@id": toa_a1}
                                    ],
                                    dt_predicate: [
                                        {"@id": dt_d1}
                                    ],
                                    cos_predicate: [
                                        {"@id": cos_c1}
                                    ]
                                }
                            }
                        ],
                        eval_predicate: [
                            {
                                "label": "Evaluation",
                                "values": {
                                    metric_predicate: [
                                        {"@id": metric_m2_p},
                                        {"@id": metric_m2_r},
                                        {"@id": metric_m2_f1},
                                        {"@id": metric_m2_ru}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)
    
    print(response.content)


if __name__ == "__main__":
    main()
