import requests
from orkg import ORKG
from habanero import Crossref
import json

orkg = ORKG('https://www.orkg.org/orkg')
vocab = dict()
cr = Crossref()

'''

Generates a literature survey for scholarly ontologies

List of ontologies from: 
Ruiz-Iniesta, Almudena, and Oscar Corcho. "A review of ontologies for describing scholarly and scientific documents." SePublica. 2014.
Additional data collected by: Oelen et al. 

'''

def main():
    ############################# PROPERTIES AND RESOURCES #############################
    #print(orkg.resources.add(label='Scholarly Ontologies').content)
    researchProblem = orkg.resources.add(label='Scholarly Ontologies').content['id']

    ontologyPredicate = orkg.predicates.add(label='Ontology').content['id']
    fullNamePredicate = orkg.predicates.add(label='Full name').content['id']
    exampleClassPredicate = orkg.predicates.add(label='Example class').content['id']
    descriptionPredicate = orkg.predicates.add(label='Description').content['id']
    classCountPredicate = orkg.predicates.add(label='Class count').content['id']
    objectPropertyCountPredicate = orkg.predicates.add(label='Object property count').content['id']
    dataPropertyCountPredicate = orkg.predicates.add(label='Data property count').content['id']
    usingOntologyPredicate = orkg.predicates.add(label='Uses ontology').content['id']
    IRIPredicate = orkg.predicates.add(label='IRI').content['id']
    WebsitePredicate = orkg.predicates.add(label='Website').content['id']


    ############################# PAPER 1,2 #############################

    paper = {
        "paper": {
            #"title": "IsaViz: a Visual Environment for Browsing and Authoring RDF Models",
            #"authors": [{"label": "E. Pietriga"}],
            #$"publicationYear": 2003,
            "doi": "10.1016/j.websem.2012.08.001",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        ontologyPredicate: [
                            {
                                "label": "FaBiO",
                                "values": {
                                    fullNamePredicate: [{"text": "FRBR-aligned Bibliographic Ontology"}],
                                    exampleClassPredicate: [
                                        {"@id": createOrFindResource(label='StorageMedium')},
                                        {"@id": createOrFindResource(label='Item')},
                                        {"@id": createOrFindResource(label='Work')},
                                        {"@id": createOrFindResource(label='Manifestation')}
                                    ],
                                    descriptionPredicate: [{"text": "Describing entities that can be published (e.g., papers). Focus on bibliographic references"}], #createOrFindResource(label='Supervised Learning')
                                    classCountPredicate: [{"text": "268"}],
                                    objectPropertyCountPredicate: [{"text": "71"}],
                                    dataPropertyCountPredicate: [{"text": "67"}],
                                    usingOntologyPredicate: [
                                        {"@id": createOrFindResource(label='FRBR')}
                                    ],
                                    IRIPredicate: [{"text": "http://purl.org/spar/fabio"}],
                                    WebsitePredicate: [{"text": "http://www.sparontologies.net/ontologies/fabio"}],
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 2",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        ontologyPredicate: [
                            {
                                "label": "CiTO",
                                "values": {
                                    fullNamePredicate: [{"text": "Citation Typing Ontology"}],
                                    descriptionPredicate: [{"text": "For describing citations, both factually and rhetorically"}],
                                    exampleClassPredicate: [
                                        {"@id": createOrFindResource(label='Citation')},
                                        {"@id": createOrFindResource(label='DistantCitation')},
                                        {"@id": createOrFindResource(label='JournalCartelCitation')},
                                        {"@id": createOrFindResource(label='SelfCitation')},
                                        #{"@id": createOrFindResource(label='')},
                                        #{"@id": createOrFindResource(label='')},
                                    ],
                                    classCountPredicate: [{"text": "11"}],
                                    objectPropertyCountPredicate: [{"text": "100"}],
                                    dataPropertyCountPredicate: [{"text": "3"}],
                                    usingOntologyPredicate: [
                                        {"@id": createOrFindResource(label='Situation')}
                                    ],
                                    IRIPredicate: [{"text": "http://purl.org/spar/cito"}],
                                    WebsitePredicate: [{"text": "http://www.sparontologies.net/ontologies/cito"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 3,4,6,7 #############################

    paper = {
        "paper": {
            "doi": "10.1007/978-3-030-00668-6_8",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        ontologyPredicate: [
                            {
                                "label": "BiRO",
                                "values": {
                                    fullNamePredicate: [{"text": "Bibliographic Reference Ontology"}],
                                    descriptionPredicate: [{"text": "Define bibliographic records, bibliographic references"}],
                                    exampleClassPredicate: [
                                        {"@id": createOrFindResource(label='BibliographicList')},
                                        {"@id": createOrFindResource(label='BibliographicReference')},
                                        {"@id": createOrFindResource(label='BibliographicCollection')},
                                        {"@id": createOrFindResource(label='BibliographicRecord')},
                                        #{"@id": createOrFindResource(label='')},
                                        #{"@id": createOrFindResource(label='')},
                                    ],
                                    #descriptionPredicate: [{"text": "Bibliographic reference"}], #createOrFindResource(label='Supervised Learning')
                                    classCountPredicate: [{"text": "26"}],
                                    objectPropertyCountPredicate: [{"text": "66"}],
                                    dataPropertyCountPredicate: [{"text": "3"}],
                                    usingOntologyPredicate: [
                                        {"@id": createOrFindResource(label='FRBR')},
                                        {"@id": createOrFindResource(label='CO')}
                                    ],
                                    IRIPredicate: [{"text": "http://purl.org/spar/biro"}],
                                    WebsitePredicate: [{"text": "http://www.sparontologies.net/ontologies/biro"}],
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 2",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        ontologyPredicate: [
                            {
                                "label": "C4O",
                                "values": {
                                    fullNamePredicate: [{"text": "Citation Counting and Context Characterisation Ontology"}],
                                    descriptionPredicate: [{"text": "Recording of in-text citations including their textual citation context"}],
                                    exampleClassPredicate: [
                                        {"@id": createOrFindResource(label='BibliographicInformationSource')},
                                        {"@id": createOrFindResource(label='GlobalCitationCount')},
                                        {"@id": createOrFindResource(label='InTextReferencePointer')},
                                        #{"@id": createOrFindResource(label='')},
                                        #{"@id": createOrFindResource(label='')},
                                        #{"@id": createOrFindResource(label='')},
                                    ],
                                    #descriptionPredicate: [{"text": "Citation counting"}], #createOrFindResource(label='Supervised Learning')
                                    classCountPredicate: [{"text": "33"}],
                                    objectPropertyCountPredicate: [{"text": "73"}],
                                    dataPropertyCountPredicate: [{"text": "8"}],
                                    usingOntologyPredicate: [
                                        {"@id": createOrFindResource(label='BiRO')}
                                    ],
                                    IRIPredicate: [{"text": "http://purl.org/spar/c4o"}],
                                    WebsitePredicate: [{"text": "http://www.sparontologies.net/ontologies/c4o"}],
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 3",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        ontologyPredicate: [
                            {
                                "label": "PRO",
                                "values": {
                                    fullNamePredicate: [{"text": "Publishing Roles Ontology"}],
                                    descriptionPredicate: [{"text": "For describing the different roles in the publication process (authors, editors, reviewers etc.)"}],
                                    exampleClassPredicate: [
                                        {"@id": createOrFindResource(label='Agent')},
                                        {"@id": createOrFindResource(label='Role')},
                                        {"@id": createOrFindResource(label='PublishingRole')},
                                        {"@id": createOrFindResource(label='RoleInTime')},
                                        #{"@id": createOrFindResource(label='')},
                                        #{"@id": createOrFindResource(label='')},
                                    ],
                                    #descriptionPredicate: [{"text": "Publishing roles"}], #createOrFindResource(label='Supervised Learning')
                                    classCountPredicate: [{"text": "39"}],
                                    objectPropertyCountPredicate: [{"text": "82"}],
                                    dataPropertyCountPredicate: [{"text": "44"}],
                                    usingOntologyPredicate: [
                                        {"@id": createOrFindResource(label='FOAF')},
                                        {"@id": createOrFindResource(label='TVC')}
                                    ],
                                    IRIPredicate: [{"text": "http://purl.org/spar/pro/"}],
                                    WebsitePredicate: [{"text": "http://www.sparontologies.net/ontologies/pro"}],
                                }
                            }
                        ],
                    }
                },
                {
                    "name": "Contribution 4",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        ontologyPredicate: [
                            {
                                "label": "PSO",
                                "values": {
                                    fullNamePredicate: [{"text": "Publishing Status Ontology"}],
                                    descriptionPredicate: [{"text": "Can record the status for the publication of a document (draft, submitted etc.)"}],
                                    exampleClassPredicate: [
                                        {"@id": createOrFindResource(label='Document')},
                                        {"@id": createOrFindResource(label='Event')},
                                        {"@id": createOrFindResource(label='PublishingStatus')},
                                        {"@id": createOrFindResource(label='Agent')},
                                        #{"@id": createOrFindResource(label='')},
                                        #{"@id": createOrFindResource(label='')},
                                    ],
                                    #descriptionPredicate: [{"text": "Publishing status"}], #createOrFindResource(label='Supervised Learning')
                                    classCountPredicate: [{"text": "43"}],
                                    objectPropertyCountPredicate: [{"text": "80"}],
                                    dataPropertyCountPredicate: [{"text": "44"}],
                                    usingOntologyPredicate: [
                                        {"@id": createOrFindResource(label='FOAF')},
                                        {"@id": createOrFindResource(label='TVC')},
                                        {"@id": createOrFindResource(label='Participation')}
                                    ],
                                    IRIPredicate: [{"text": "http://purl.org/spar/pso"}],
                                    WebsitePredicate: [{"text": "http://www.sparontologies.net/ontologies/pso"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 5 #############################

    paper = {
        "paper": {
            "doi": "10.3233/SW-150177",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        ontologyPredicate: [
                            {
                                "label": "DoCO",
                                "values": {
                                    fullNamePredicate: [{"text": "Document Components Ontology"}],
                                    descriptionPredicate: [{"text": "For document structure (paragraph, chapter etc.) and rhetorical elements (introduction, discussion)"}],
                                    exampleClassPredicate: [
                                        {"@id": createOrFindResource(label='Abstract')},
                                        {"@id": createOrFindResource(label='Bibliography')},
                                        {"@id": createOrFindResource(label='Figure')},
                                        {"@id": createOrFindResource(label='Footnote')},
                                        {"@id": createOrFindResource(label='Paragraph')}
                                        #{"@id": createOrFindResource(label='')},
                                        #{"@id": createOrFindResource(label='')},
                                    ],
                                    #descriptionPredicate: [{"text": "Document Components"}], #createOrFindResource(label='Supervised Learning')
                                    classCountPredicate: [{"text": "122"}],
                                    objectPropertyCountPredicate: [{"text": "8"}],
                                    dataPropertyCountPredicate: [{"text": "3"}],
                                    usingOntologyPredicate: [
                                        {"@id": createOrFindResource(label='DEO')},
                                        {"@id": createOrFindResource(label='PO')}
                                    ],
                                    IRIPredicate: [{"text": "http://purl.org/spar/doco"}],
                                    WebsitePredicate: [{"text": "http://www.sparontologies.net/ontologies/doco"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 8 #############################

    paper = {
        "paper": {
            "doi": "10.3233/SW-160230",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        ontologyPredicate: [
                            {
                                "label": "PWO",
                                "values": {
                                    fullNamePredicate: [{"text": "Publishing Workflow Ontology"}],
                                    descriptionPredicate: [{"text": "Used for describing the workflow steps for the publication of a document"}],
                                    exampleClassPredicate: [
                                        {"@id": createOrFindResource(label='Event')},
                                        {"@id": createOrFindResource(label='Agent')},
                                        {"@id": createOrFindResource(label='Situation')},
                                        {"@id": createOrFindResource(label='TempopralPosition')},
                                        {"@id": createOrFindResource(label='TimeZone')}
                                    ],
                                    #descriptionPredicate: [{"text": "Publishing workflow"}], #createOrFindResource(label='Supervised Learning')
                                    classCountPredicate: [{"text": "65"}],
                                    objectPropertyCountPredicate: [{"text": "102"}],
                                    dataPropertyCountPredicate: [{"text": "31"}],
                                    usingOntologyPredicate: [
                                        {"@id": createOrFindResource(label='basicplan')},
                                        {"@id": createOrFindResource(label='controlflow')},
                                        {"@id": createOrFindResource(label='participation')},
                                        {"@id": createOrFindResource(label='sequence')},
                                        {"@id": createOrFindResource(label='timeindexed')},
                                        {"@id": createOrFindResource(label='situation')},
                                        {"@id": createOrFindResource(label='timeinterval')},
                                        {"@id": createOrFindResource(label='time')}
                                    ],
                                    IRIPredicate: [{"text": "http://purl.org/spar/pwo"}],
                                    WebsitePredicate: [{"text": "http://www.sparontologies.net/ontologies/pwo"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

    ############################# PAPER 9 #############################

    paper = {
        "paper": {
            "doi": "10.1007/s007990000034",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        ontologyPredicate: [
                            {
                                "label": "ScholOnto",
                                "values": {
                                    descriptionPredicate: [{"text": "For scholarly interpretation and discourse. Describe debate and relations to the literature"}],
                                    exampleClassPredicate: [
                                        {"@id": createOrFindResource(label='RhetProperty')},
                                        {"@id": createOrFindResource(label='WeightType')},
                                        {"@id": createOrFindResource(label='PolarityType')},
                                        #{"@id": createOrFindResource(label='')},
                                        #{"@id": createOrFindResource(label='')},
                                    ],
                                    #descriptionPredicate: [{"text": "Scholarly discourse"}], #createOrFindResource(label='Supervised Learning')
                                    classCountPredicate: [{"text": "29"}],
                                    objectPropertyCountPredicate: [{"text": "44"}],
                                    dataPropertyCountPredicate: [{"text": "6"}],
                                    WebsitePredicate: [{"text": "http://projects.kmi.open.ac.uk/scholonto"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)

   ############################# PAPER 10 #############################

    paper = {
        "paper": {
            "title": "Ontology of Rhetorical Blocks (ORB)",
            "publicationYear": 2011,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        ontologyPredicate: [
                            {
                                "label": "ORB",
                                "values": {
                                    fullNamePredicate: [{"text": "Ontology of Rhetorical Blocks"}],
                                    descriptionPredicate: [{"text": "Coarse-grained rhetorical scholarly document structure"}],
                                    exampleClassPredicate: [
                                        {"@id": createOrFindResource(label='Discussion')},
                                        {"@id": createOrFindResource(label='Header')},
                                        {"@id": createOrFindResource(label='Introduction')},
                                        {"@id": createOrFindResource(label='Results')},
                                        #{"@id": createOrFindResource(label='')},
                                        #{"@id": createOrFindResource(label='')},
                                    ],
                                    #descriptionPredicate: [{"text": "Coarse-grained rhetorical scholarly document structure"}], #createOrFindResource(label='Supervised Learning')
                                    classCountPredicate: [{"text": "7"}],
                                    objectPropertyCountPredicate: [{"text": "0"}],
                                    dataPropertyCountPredicate: [{"text": "0"}],
                                    IRIPredicate: [{"text": "http://purl.org/orb"}],
                                    WebsitePredicate: [{"text": "https://www.w3.org/TR/hcls-orb"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)
    
    ############################# PAPER 12 #############################

    paper = {
        "paper": {
            "doi": "10.1098/rsif.2006.0134",
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        ontologyPredicate: [
                            {
                                "label": "EXPO",
                                "values": {
                                    descriptionPredicate: [{"text": "Ontology for describing scientific experiments, including the design, methodology and results"}],
                                    exampleClassPredicate: [
                                        {"@id": createOrFindResource(label='Entity')},
                                        {"@id": createOrFindResource(label='Abstract')},
                                        {"@id": createOrFindResource(label='Physical')},
                                        {"@id": createOrFindResource(label='Proposition')},
                                        #{"@id": createOrFindResource(label='')},
                                        #{"@id": createOrFindResource(label='')},
                                    ],
                                    #descriptionPredicate: [{"text": "Experiment ontology"}], #createOrFindResource(label='Supervised Learning')
                                    classCountPredicate: [{"text": "325"}],
                                    objectPropertyCountPredicate: [{"text": "78"}],
                                    dataPropertyCountPredicate: [{"text": "0"}],
                                    WebsitePredicate: [{"text": "http://expo.sourceforge.net"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)


    ############################# PAPER 15 #############################

    paper = {
        "paper": {
            "title": "Bibliographic ontology specification",
            "authors": [{"label": "D’Arcus, B."}, {"label": "Giasson, F."}],
            "publicationYear": 2009,
            "researchField": "R136",
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": researchProblem}
                        ],
                        ontologyPredicate: [
                            {
                                "label": "BIBO",
                                "values": {
                                    fullNamePredicate: [{"text": "Bibliographic Ontology"}],
                                    descriptionPredicate: [{"text": "Describing  citations and bibliographic references (including quotes, books and articles)"}],
                                    exampleClassPredicate: [
                                        {"@id": createOrFindResource(label='Collection')},
                                        {"@id": createOrFindResource(label='Document')},
                                        {"@id": createOrFindResource(label='Agent')},
                                        {"@id": createOrFindResource(label='Event')},
                                        #{"@id": createOrFindResource(label='')},
                                        #{"@id": createOrFindResource(label='')},
                                    ],
                                    #descriptionPredicate: [{"text": "Bibliographic entities"}], #createOrFindResource(label='Supervised Learning')
                                    classCountPredicate: [{"text": "70"}],
                                    objectPropertyCountPredicate: [{"text": "53"}],
                                    dataPropertyCountPredicate: [{"text": "55"}],
                                    usingOntologyPredicate: [
                                        {"@id": createOrFindResource(label='FOAF')},
                                        {"@id": createOrFindResource(label='Dublin Core')},
                                    ],
                                    IRIPredicate: [{"text": "http://purl.org/ontology/bibo"}],
                                    WebsitePredicate: [{"text": "http://bibliontology.com/"}],
                                }
                            }
                        ],
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)

    response1 = orkg.papers.add(paper)

    print(response1.content)


def doiLookup(paper): 
    if ("doi" in paper['paper']):
        lookupResult = cr.works(ids = paper['paper']['doi'])
        if (lookupResult['status'] == 'ok'):
            print(lookupResult['message']['published-print']['date-parts'][0])
            paper['paper']['title'] = lookupResult['message']['title'][0]
            
            if (len(lookupResult['message']['published-print']['date-parts'][0]) > 0):
                paper['paper']['publicationYear'] = lookupResult['message']['published-print']['date-parts'][0][0]

            if (len(lookupResult['message']['published-print']['date-parts'][0]) > 1):
                paper['paper']['publicationMonth'] = lookupResult['message']['published-print']['date-parts'][0][1]

            if (len(lookupResult['message']['author']) > 0): 
                paper['paper']['authors'] = []
                for author in lookupResult['message']['author']: 
                    paper['paper']['authors'].append({"label": author['given'] + ' ' + author['family']})

    return paper


def createOrFindResource(label):
    findResource = orkg.resources.get(q=label, exact=True).content
    if (len(findResource) > 0):
        resource = findResource[0]['id']
    else:
        resource = orkg.resources.add(label=label).content['id']
    return resource



if __name__ == "__main__":
    main()
