from habanero import Crossref
from orkg import ORKG

orkg = ORKG()
cr = Crossref()


def createClassIfNotExist(id, label):
    findClass = orkg.classes.by_id(id).content
    if ('status' in findClass and findClass['status'] == 404):
        orkg.classes.add(id=id, label=label)


def createPredicateIfNotExist(id, label):
    findPredicate = orkg.predicates.by_id(id).content
    if ('status' in findPredicate and findPredicate['status'] == 404):
        orkg.predicates.add(id=id, label=label)


def createOrFindPredicate(label):
    findPredicate = orkg.predicates.get(q=label, exact=True).content
    if (len(findPredicate) > 0):
        predicate = findPredicate[0]['id']
    else:
        predicate = orkg.predicates.add(label=label).content['id']
    return predicate


def createOrFindResource(label, classes=[]):
    findResource = orkg.resources.get(q=label, exact=True).content
    if (len(findResource) > 0):
        resource = findResource[0]['id']
    else:
        resource = orkg.resources.add(label=label, classes=classes).content['id']
    return resource


def doiLookup(paper):
    if ("doi" in paper['paper']):
        lookupResult = cr.works(ids=paper['paper']['doi'])
        if (lookupResult['status'] == 'ok'):
            paper['paper']['title'] = lookupResult['message']['title'][0]
            if 'published-print' in lookupResult['message']:
                if (len(lookupResult['message']['published-print']['date-parts'][0]) > 0):
                    paper['paper']['publicationYear'] = lookupResult['message']['published-print']['date-parts'][0][0]

                if (len(lookupResult['message']['published-print']['date-parts'][0]) > 1):
                    paper['paper']['publicationMonth'] = lookupResult['message']['published-print']['date-parts'][0][1]
            elif 'created' in lookupResult['message']:
                if (len(lookupResult['message']['created']['date-parts'][0]) > 0):
                    paper['paper']['publicationYear'] = lookupResult['message']['created']['date-parts'][0][0]

                if (len(lookupResult['message']['created']['date-parts'][0]) > 1):
                    paper['paper']['publicationMonth'] = lookupResult['message']['created']['date-parts'][0][1]

            if (len(lookupResult['message']['author']) > 0):
                paper['paper']['authors'] = []
                for author in lookupResult['message']['author']:
                    paper['paper']['authors'].append(
                        {"label": author['given'] + ' ' + author['family']})
    return paper


'''

Imports the author name disambiguation comparison

Data from: 
Hussain, I., & Asghar, S. (2017). A survey of author name disambiguation techniques: 2010–2016. 
The Knowledge Engineering Review, 32, E22. doi:10.1017/S0269888917000182


'''


def main():
    createClassIfNotExist(id='Paper', label='Paper')
    createClassIfNotExist(id='Problem', label='Problem')
    createClassIfNotExist(id='Contribution', label='Contribution')
    createClassIfNotExist(id='Author', label='Author')
    createPredicateIfNotExist(id='HAS_ORCID', label='HAS ORCID')

    ############################# PROPERTIES AND RESOURCES #############################
    nameAmbiguityProblem = createOrFindResource(label='Author name disambiguation', classes=['Problem'])

    # Categorie
    usedApproachPredicate = createOrFindPredicate(label='Approach')
    supervisedRessource = createOrFindResource(label='Supervised Learning')
    unsupervisedRessource = createOrFindResource(label='Unsupervised Learning')
    semisupervisedRessource = createOrFindResource(
        label='Semi-Supervised Learning')
    graphBasedRessource = createOrFindResource(label='Graph Approach')
    heuristicRessource = createOrFindResource(label='Heuristic Approach')

    usedMethodPredicate = createOrFindPredicate(label='method')
    followedHeuristicPredicate = createOrFindPredicate(
        label='Heuristic followed')

    # Capability
    homonymsProblem = createOrFindResource(label='Homonyms problem')
    synonymsProblem = createOrFindResource(label='Synonyms problem')
    capabilityPredicate = createOrFindPredicate(label='deals with')

    similarityPredicate = createOrFindPredicate(label='uses similarity')
    performanceMetricPredicate = createOrFindPredicate(
        label='Performance metric')
    datasetPredicate = createOrFindPredicate(label='Dataset')
    evidencePredicate = createOrFindPredicate(label='Evidence')
    uncertaintyPredicate = createOrFindPredicate(label='Uncertainty')
    limitationsPredicate = createOrFindPredicate(label='Limitations')

    clusteringPredicate = createOrFindPredicate(label='Clustering')
    graphPredicate = createOrFindPredicate(label='Graph')

    ############################# PAPER #############################
    # Wang et al. (2012)
    paper = {
        "paper": {
            "doi": "10.1007/s11192-012-0681-1",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": supervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('Cosine')},
                            {"@id": createOrFindResource(
                                'Eigen Decomposition')}
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource(
                                'Boosted tree classification')}
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Precision')},
                            {"@id": createOrFindResource('Recall')},
                            {"@id": createOrFindResource(
                                'Misclassification error rate')}
                        ],
                        uncertaintyPredicate: [
                            {"text": "F"}
                        ],
                        datasetPredicate: [
                            {"text": "Not standard"}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Author name')},
                            {"@id": createOrFindResource('Venues')},
                            {"@id": createOrFindResource('Keywords')},
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Abstract')},
                            {"@id": createOrFindResource('Subject category')}
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "How to decide the splitting point and how to control the size of the tree."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Han et al. (2015)

    paper = {
        "paper": {
            "doi": "10.1007/s11280-013-0226-4",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": supervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('TF-IDF')}
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource(
                                'Extreme Learning Machine')}
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource(
                                'Mean of test accuracy')},
                            {"@id": createOrFindResource(
                                'Standard Deviation of test accuracy')},
                        ],
                        uncertaintyPredicate: [
                            {"text": "F"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource("Self-designed DBLP")}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Author name')},
                            {"@id": createOrFindResource('Co-authors')},
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Venues')}
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "It is difficult to decide network structure and its parameters."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Tran et al. (2014)

    paper = {
        "paper": {
            "doi": "10.1007/978-3-319-05476-6_13",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": supervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('Jaccard')},
                            {"@id": createOrFindResource('Jaro')},
                            {"@id": createOrFindResource(
                                'Levenshtein distance')},
                            {"@id": createOrFindResource('Smith–Waterman')},
                            {"@id": createOrFindResource('Jaro–Winkler')},
                            {"@id": createOrFindResource('Monge-Elkan')}
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource(
                                'Deep neural network')}
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Accuracy')},
                            {"@id": createOrFindResource('Error rate')},
                        ],
                        uncertaintyPredicate: [
                            {"text": "T"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource(
                                "IEEE Explore Vietnamese authors")},
                            {"@id": createOrFindResource(
                                "Microsoft Academic Service")},
                            {"@id": createOrFindResource(
                                "Association for Computing Machinery")},
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Author name')},
                            {"@id": createOrFindResource('Co-authors')},
                            {"@id": createOrFindResource('Affiliation')},
                            {"@id": createOrFindResource('Interest')},
                            {"@id": createOrFindResource('Keywords')}
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "It requires retraining the model if some parameters are changed. Many models for each author."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Onodera et al. (2011)

    paper = {
        "paper": {
            "doi": "10.1002/asi.21491",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": supervisedRessource}
                        ],
                        similarityPredicate: [
                            {"text": "Self-defined"}
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource(
                                'Logistic regression')}
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Precision')},
                            {"@id": createOrFindResource('Recall')},
                            {"@id": createOrFindResource('Accuracy')},
                        ],
                        uncertaintyPredicate: [
                            {"text": "F"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource(
                                "Web of Science")}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Co-authors')},
                            {"@id": createOrFindResource('Affiliation')},
                            {"@id": createOrFindResource(
                                'Citation relationship')},
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Co-citations')},
                            {"@id": createOrFindResource('Publication year')},
                            {"@id": createOrFindResource('Country')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Cannot solve transitivity problem. Fails if subject fields and affiliation do not vary too much."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Huynh et al. (2013)

    paper = {
        "paper": {
            "doi": "10.1007/978-3-642-36546-1_24",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": supervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('Jaccard')}
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource('Random forest')},
                            {"@id": createOrFindResource(
                                'Support Vector Machine')},
                            {"@id": createOrFindResource(
                                'K-nearest neighbors')},
                            {"@id": createOrFindResource('Decision trees')},
                            {"@id": createOrFindResource('Bayes')}
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Accuracy')}
                        ],
                        uncertaintyPredicate: [
                            {"text": "F"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource(
                                "ACM Vietnamese Dataset")},
                            {"@id": createOrFindResource("IEEE")},
                            {"@id": createOrFindResource(
                                "Microsoft Academic Service")}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Author name')},
                            {"@id": createOrFindResource('Co-authors')},
                            {"@id": createOrFindResource('Affiliation')},
                            {"@id": createOrFindResource(
                                'Keywords similarity')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Cannot handle outliers."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    #################################################################
    # Wu et al. (2014)

    paper = {
        "paper": {
            "doi": "10.1007/s11192-014-1283-x",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": unsupervisedRessource}
                        ],
                        clusteringPredicate: [
                            {"@id": createOrFindResource(
                                'Hierarchical agglomerative')},
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('Jaccard')},
                            {"@id": createOrFindResource(
                                'Levenshtein distance')},
                            {"@id": createOrFindResource('Cosine')}
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource(
                                'Dempster–Shafer theory')},
                            {"@id": createOrFindResource('Shannon entropy')}
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('K metric')},
                            {"@id": createOrFindResource('Pairwise F1')}
                        ],
                        uncertaintyPredicate: [
                            {"text": "T"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource("DBLP")}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Co-authors')},
                            {"@id": createOrFindResource('Affiliation')},
                            {"@id": createOrFindResource('Venues')},
                            {"@id": createOrFindResource(
                                'Content Similarity')},
                            {"@id": createOrFindResource('Citations')},
                            {"@id": createOrFindResource('Web co-relations')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Low accuracy."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Schulz et al. (2014)

    paper = {
        "paper": {
            "doi": "10.1140/epjds/s13688-014-0011-3",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": unsupervisedRessource}
                        ],
                        clusteringPredicate: [
                            {"@id": createOrFindResource(
                                '2-Step agglomerative')},
                        ],
                        similarityPredicate: [
                            {"text": 'Own defined'}
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource(
                                '2-step agglomerative clustering')},
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Precision')},
                            {"@id": createOrFindResource('H-index error')}
                        ],
                        uncertaintyPredicate: [
                            {"text": "T"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource("Web of Science")}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Author name')},
                            {"@id": createOrFindResource('Shared citations')},
                            {"@id": createOrFindResource('Self-citations')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "H-index is not known for new authors."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Tang and Walsh (2010)

    paper = {
        "paper": {
            "doi": "10.1007/s11192-010-0196-6",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": unsupervisedRessource}
                        ],
                        clusteringPredicate: [
                            {"@id": createOrFindResource(
                                'Hierarchical agglomerative')},
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource(
                                'Knowledge homogeneity score (KHS)')},
                            {"text": 'Self-defined similarity'},
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource(
                                'Approximate Structure equivalence-based KHS')},
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Accuracy')}
                        ],
                        uncertaintyPredicate: [
                            {"text": "F"}
                        ],
                        datasetPredicate: [
                            {"text": 'Limited (own designed)'}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Author name')},
                            {"@id": createOrFindResource('References')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Homogeneity threshold varies from subject to subject"}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Tang et al. (2012)

    paper = {
        "paper": {
            "doi": "10.1109/TKDE.2011.13",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": unsupervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('Cosine')},
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource(
                                ' Unified Probabilistic framework with Markov random fields')},
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Pairwise accuracy')},
                            {"@id": createOrFindResource('Precision')},
                            {"@id": createOrFindResource('F1')}
                        ],
                        uncertaintyPredicate: [
                            {"text": "F"}
                        ],
                        datasetPredicate: [
                            {"text": 'Self designed'}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Author name')},
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Abstract')},
                            {"@id": createOrFindResource('References')},
                            {"@id": createOrFindResource('Year')},
                            {"@id": createOrFindResource('Venue')}
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "If relationship information is not available then this method would not perform well"}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # De Carvalho et al. (2011)

    paper = {
        "paper": {
            "title": "Incremental unsupervised name disambiguation in cleaned digital libraries.",
            "authors": [{"label": "Ana Paula de	Carvalho"}, {"label": "Anderson Almeida Ferreira"}, {"label": "Alberto Henrique Frade Laender"}, {"label": "Marcos André Gonçalves"}],
            "publicationYear": 2011,
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": unsupervisedRessource}
                        ],
                        clusteringPredicate: [
                            {"@id": createOrFindResource(
                                'Hierarchical')},
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource(
                                'Fragment comparison')},
                            {"@id": createOrFindResource('Cosine')},
                            {"@id": createOrFindResource('TF-IDF')},
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource('Heuristic based')},
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('K metric')}
                        ],
                        uncertaintyPredicate: [
                            {"text": "T"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource('BDBComp')},
                            {"text": 'Synthetic Dataset'}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Author name')},
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Venues')}
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "It fails if similar author name but does not have similarity, not have co-authors and not match with any existing cluster"}
                        ]
                    }
                },
                {
                    "name": "Contribution 2",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": heuristicRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('String Comparison')}
                        ],
                        followedHeuristicPredicate: [
                            {"@id": createOrFindResource(
                                'Publication features')}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource("BDBComp")}
                        ],
                        limitationsPredicate: [
                            {"text": "No real world data set is used."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Liu et al. (2015)

    paper = {
        "paper": {
            "doi": "10.1002/asi.23183",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": unsupervisedRessource}
                        ],
                        clusteringPredicate: [
                            {"@id": createOrFindResource(
                                'Hierarchical')},
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('Cosine')},
                        ],
                        usedMethodPredicate: [
                            {"text": 'Hierarchical clusters of relations between authors and papers'},
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource(
                                'Pairwise Precision')},
                            {"@id": createOrFindResource('Pairwise Recall')},
                            {"@id": createOrFindResource('Pairwise F1')},
                        ],
                        uncertaintyPredicate: [
                            {"text": "F"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource('DBLP')}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Author name')},
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Venues')}
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "It fails if authors with the same name have similar research fields"}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Zhu et al. (2014)

    paper = {
        "paper": {
            "doi": "10.1007/s11192-013-1151-0",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": semisupervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource(
                                'Multi-dimensional scaling')},
                        ],
                        usedMethodPredicate: [
                            {"text": 'Web page genre identification based graph re-clustering'},
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('F1')},
                        ],
                        uncertaintyPredicate: [
                            {"text": "Robust"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource('DBLP')}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource(
                                'Explicit web genre')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Slow as using web information"}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Gurney et al. (2012)

    paper = {
        "paper": {
            "doi": "10.1007/s11192-011-0589-1",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": semisupervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource(
                                'Tani Moto coefficient')},
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource(
                                'Logistic regression')},
                            {"@id": createOrFindResource(
                                'Community detection algorithm')}
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Precision')},
                            {"@id": createOrFindResource('Recall')},
                            {"@id": createOrFindResource('F-measure')},
                        ],
                        uncertaintyPredicate: [
                            {"text": "T"}
                        ],
                        datasetPredicate: [
                            {"text": 'Self-designed WoS'}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Abstract')},
                            {"@id": createOrFindResource('Keywords')},
                            {"@id": createOrFindResource('Citations')},
                            {"text": 'Difference in years of publication'},
                            {"text": 'Average author contribution'},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Requires too many parameters"}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Imran et al. (2013)

    paper = {
        "paper": {
            "doi": "10.1045/september2013-imran",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": semisupervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('Levenshetien')},
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource('Heuristic-based')},
                            {"@id": createOrFindResource(
                                'Unsupervised and Adaptive')}
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Precision')},
                            {"@id": createOrFindResource('Recall')},
                            {"@id": createOrFindResource('F1')},
                        ],
                        uncertaintyPredicate: [
                            {"text": "F"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource('DBLP')},
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Authors')},
                            {"@id": createOrFindResource('Affiliation')},
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Venues')},
                            {"@id": createOrFindResource('Home page')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "It involves user on multiple stages"}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Zhao et al. (2013)

    paper = {
        "paper": {
            "doi": "10.1145/2517288.2517298",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": semisupervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('Cosine')},
                            {"@id": createOrFindResource('TF-IDF')},
                            {"@id": createOrFindResource(
                                'Levenshtein distance')},
                            {"@id": createOrFindResource('Tanimoto')},
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource(
                                'Community detection algorithm')},
                            {"@id": createOrFindResource(
                                'Support Vector Machine')},
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Precision')},
                            {"@id": createOrFindResource('Recall')},
                            {"@id": createOrFindResource('F1')},
                        ],
                        uncertaintyPredicate: [
                            {"text": "T"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource(
                                "Microsoft Academic Service")},
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Venues')},
                            {"@id": createOrFindResource('Keywords')},
                            {"@id": createOrFindResource('Affiliation')},
                            {"@id": createOrFindResource('Publication year')},

                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Complex rules and topic modeling needed"}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Maguire (2016)

    paper = {
        "paper": {
            "doi": "10.1007/978-3-319-45880-9_21",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": semisupervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('Cosine')},
                            {"@id": createOrFindResource('Jaro–Winkler')},
                            {"@id": createOrFindResource('TF-IDF')},
                        ],
                        usedMethodPredicate: [
                            {"text": 'First gradient boosted tree applied on similar authors and then agglomerative clustering is performed'},
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource(
                                'Pairwise precision')},
                            {"@id": createOrFindResource('Recall')},
                            {"@id": createOrFindResource('F1')},
                        ],
                        uncertaintyPredicate: [
                            {"text": "F"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource("INSPIRE")},
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Authors')},
                            {"@id": createOrFindResource('Affiliation')},
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Venues')},
                            {"@id": createOrFindResource('Abstract')},
                            {"@id": createOrFindResource('Keywords')},
                            {"@id": createOrFindResource('Collaborations')},
                            {"@id": createOrFindResource('References')},
                            {"@id": createOrFindResource('Subject')},
                            {"@id": createOrFindResource('Year difference')}
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Not applicable to all DL’s data set"}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Peng et al. (2012)

    paper = {
        "paper": {
            "doi": "10.1016/j.eswa.2012.02.121",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": semisupervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource(
                                'Modified sigmoid function')},
                            {"@id": createOrFindResource('Cosine')},
                            {"@id": createOrFindResource(
                                'Name popularity measure')},
                        ],
                        usedMethodPredicate: [
                            {"text": 'Web and authorship correlations'},
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource(
                                'Pairwise precision')},
                            {"@id": createOrFindResource('Recall')},
                            {"@id": createOrFindResource('F1')},
                        ],
                        uncertaintyPredicate: [
                            {"text": "F"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource("DBLP")},
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Authors')},
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Venues')}
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Inherently slow as using web info"}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Levin et al. (2012)

    paper = {
        "paper": {
            "doi": "10.1002/asi.22621",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": semisupervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('TF-IDF')},
                            {"@id": createOrFindResource('Cosine')},
                        ],
                        usedMethodPredicate: [
                            {"text": 'Self-citation clustering rules with other rules'},
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource(
                                'Pairwise precision')},
                            {"@id": createOrFindResource('Recall')},
                            {"@id": createOrFindResource('F1')},
                        ],
                        uncertaintyPredicate: [
                            {"text": "F"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource(
                                "WoK Thomson Reuters")},
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Venues')},
                            {"@id": createOrFindResource('Authors')},
                            {"@id": createOrFindResource('Addresses')},
                            {"@id": createOrFindResource('Affiliation')},
                            {"@id": createOrFindResource(
                                'Subject categories')},
                            {"@id": createOrFindResource(
                                'Citations languages')},
                            {"@id": createOrFindResource('Year')},
                            {"text": 'Combinations of these'},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Too much processing for large set of features"}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Ferreira et al. (2014)

    paper = {
        "paper": {
            "doi": "10.1002/asi.22992",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": semisupervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('Cosine')},
                            {"@id": createOrFindResource('Euclidean')},
                        ],
                        usedMethodPredicate: [
                            {"text": 'First pure clusters of data are found and model is trained, tested on them'},
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('K metric')},
                            {"@id": createOrFindResource('Pairwise F1')},
                        ],
                        uncertaintyPredicate: [
                            {"text": "F"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource("DBLP")},
                            {"@id": createOrFindResource("BDBComp")},
                            {"@id": createOrFindResource("SyGAR")},
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Authors')},
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Venues')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Threshold selection is a big issue"}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Wang et al. (2014)

    paper = {
        "paper": {
            "doi": "10.1007/978-3-319-10085-2_1",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": semisupervisedRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('Cosine')},
                            {"@id": createOrFindResource('Tanimoto')},
                            {"@id": createOrFindResource('TF-IDF')},
                        ],
                        usedMethodPredicate: [
                            {"text": 'Similar authors share co-authors and have high topic similarity'},
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('F1')},
                        ],
                        uncertaintyPredicate: [
                            {"text": "F"}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource(
                                "Microsoft Academic Service")},
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Affiliation')},
                            {"@id": createOrFindResource('Keywords')},
                            {"@id": createOrFindResource('Venues')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Fails in case of sole authors"}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    #################################################################
    # On et al. (2012)

    paper = {
        "paper": {
            "doi": "10.1007/s10115-011-0397-1",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": graphBasedRessource}
                        ],
                        graphPredicate: [
                            {"@id": createOrFindResource('Co-authorship')},
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('TF-IDF')}
                        ],
                        usedMethodPredicate: [
                            {"text": 'Graph partitioning and merging'}
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Precision')},
                            {"@id": createOrFindResource('Recall')},
                            {"@id": createOrFindResource('F1')}
                        ],
                        datasetPredicate: [
                            {"text": "4 Small to large Dataset"}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Documents')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Number of Clusters Required."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Shin et al. (2014)

    paper = {
        "paper": {
            "doi": "10.1007/s11192-014-1289-4",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": graphBasedRessource}
                        ],
                        graphPredicate: [
                            {"@id": createOrFindResource('Co-authorship')},
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource(
                                'Longest Common Subsequence')},
                            {"@id": createOrFindResource('Cosine')}
                        ],
                        usedMethodPredicate: [
                            {"text": 'Graph Node Splitting & Merging'}
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('K metric')},
                            {"@id": createOrFindResource('Pairwise F1')},
                            {"@id": createOrFindResource('Cluster F1')}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource('DBLP')},
                            {"@id": createOrFindResource('Arnetminer')}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Co-authors')},
                            {"@id": createOrFindResource('Title words')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Unable to disambiguate sole authors."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Fan et al. (2011)

    paper = {
        "paper": {
            "doi": "10.1145/1891879.1891883",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": graphBasedRessource}
                        ],
                        graphPredicate: [
                            {"@id": createOrFindResource('Co-authorship')},
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('User defined')},
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource(
                                'Affinity propagation clustering')}
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Pairwise accuracy')},
                            {"@id": createOrFindResource('Precision')},
                            {"@id": createOrFindResource('F1')}
                        ],
                        datasetPredicate: [
                            {"text": 'Small standard DS'}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Co-author graphs')},
                            {"@id": createOrFindResource('User feedback')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Slow due to user feedback."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Levin and Heuser (2010)

    paper = {
        "paper": {
            "title": "Evaluating the Use of Social Networks in Author Name Disambiguation in Digital Libraries",
            "authors": [{"label": "Felipe Hoppe Levin"}, {"label": "Carlos A. Heuser"}],
            "publicationYear": 2010,
            "publicationMonth": 9,
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": graphBasedRessource}
                        ],
                        graphPredicate: [
                            {"@id": createOrFindResource(
                                'Author social graph')},
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource(
                                'Levenshtein distance')},
                            {"@id": createOrFindResource('Trigram')}
                        ],
                        usedMethodPredicate: [
                            {"text": 'Syntactic relationship match function'}
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Accuracy')},
                            {"@id": createOrFindResource('Precision')},
                            {"@id": createOrFindResource('F1')}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource('Cora')},
                            {"@id": createOrFindResource('BDBComp')},
                            {"@id": createOrFindResource('DBLP')}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Authors')},
                            {"@id": createOrFindResource('Title words')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "If no relationship or low syntactic similarity then fails."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Wang et al. (2011)

    paper = {
        "paper": {
            "doi": "10.1109/ICDM.2011.19",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": graphBasedRessource}
                        ],
                        graphPredicate: [
                            {"@id": createOrFindResource(
                                'Pairwise Factor Graph')},
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('TF-IDF')},
                            {"@id": createOrFindResource('Cosine')},
                        ],
                        usedMethodPredicate: [
                            {"@id": createOrFindResource(
                                'Pairwise Factor Graph')},
                            {"text": 'interaction of user to enhance the performance'}
                        ],
                        performanceMetricPredicate: [
                            {"@id": createOrFindResource('Precision')},
                            {"@id": createOrFindResource('Recall')},
                            {"@id": createOrFindResource('F-measure')}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource('Publication')},
                            {"@id": createOrFindResource('Web page')},
                            {"@id": createOrFindResource('News stories')}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Citations')},
                            {"@id": createOrFindResource('Co-Authors')},
                            {"@id": createOrFindResource('Co-Venues')},
                            {"@id": createOrFindResource('Co-Affiliation')},
                            {"@id": createOrFindResource(
                                'Co-Affiliation-occur')},
                            {"@id": createOrFindResource('Title word-sim')},
                            {"@id": createOrFindResource('Co-Home page')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Not scalable due to huge number of path calculations."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Liu and Tang (2015)

    paper = {
        "paper": {
            "doi": "10.14257/ijunesst.2015.8.9.09",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": graphBasedRessource}
                        ],
                        graphPredicate: [
                            {"@id": createOrFindResource('Bi-relational')},
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource(
                                'Graph-based closeness')},
                        ],
                        usedMethodPredicate: [
                            {"text": 'Bi-relational network is created, closeness between nodes is Found for clustering'}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource('Citations')}
                        ],
                        evidencePredicate: [
                            {"@id": createOrFindResource('Authors')},
                            {"@id": createOrFindResource('Title words')},
                            {"@id": createOrFindResource('Venues')},
                        ],
                        capabilityPredicate: [
                            {"@id": homonymsProblem},
                            {"@id": synonymsProblem}
                        ],
                        limitationsPredicate: [
                            {"text": "Knowledge base is required that is not available in some DL"}
                        ]
                    }
                },
                {
                    "name": "Contribution 2",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": heuristicRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource(
                                'Levenshtein distance')},
                            {"@id": createOrFindResource('Soundex')},
                        ],
                        followedHeuristicPredicate: [
                            {"@id": createOrFindResource(
                                'Meta path-based ranking')}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource(
                                "Microsoft Academic Service")},
                        ],
                        limitationsPredicate: [
                            {"text": "It fails on sole authors cases."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    #################################################################
    # Chin et al. (2014)

    paper = {
        "paper": {
            "title": "Effective String Processing and Matching for Author Disambiguation",
            "authors": [
                {
                    "label": "Wei-Sheng Chin"
                },
                {
                    "label": "Yong Zhuang"
                },
                {
                    "label": "Yu-Chin Juan"
                },
                {
                    "label": "Felix Wu"
                },
                {
                    "label": "Hsiao-Yu Tung"
                },
                {
                    "label": "Tong Yu"
                },
                {
                    "label": "Jui-Pin Wang"
                },
                {
                    "label": "Cheng-Xia Chang"
                },
                {
                    "label": "Chun-Pai Yang"
                },
                {
                    "label": "Wei-Cheng Chang"
                },
                {
                    "label": "Kuan-Hao Huang"
                },
                {
                    "label": "Tzu-Ming Kuo"
                },
                {
                    "label": "Shan-Wei Lin"
                },
                {
                    "label": "Young-San Lin"
                },
                {
                    "label": "Yu-Chen Lu"
                },
                {
                    "label": "Yu-Chuan Su"
                },
                {
                    "label": "Cheng-Kuang Wei"
                },
                {
                    "label": "Tu-Chun Yin"
                },
                {
                    "label": "Chun-Liang Li"
                },
                {
                    "label": "Ting-Wei Lin"
                },
                {
                    "label": "Cheng-Hao Tsai"
                },
                {
                    "label": "Shou-De Lin"
                },
                {
                    "label": "Hsuan-Tien Lin"
                },
                {
                    "label": "Chih-Jen Lin"
                }
            ],
            "publicationYear": 2014,
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": heuristicRessource}
                        ],
                        similarityPredicate: [
                            {"@id": createOrFindResource('String comparison')}
                        ],
                        followedHeuristicPredicate: [
                            {"@id": createOrFindResource('String processing')}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource(
                                "Microsoft Academic Service")},
                        ],
                        limitationsPredicate: [
                            {"text": "It only finds duplicates. Not tell the No. of ambiguous authors."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)

    ############################# PAPER #############################
    # Santana et al. (2015)

    paper = {
        "paper": {
            "doi": "10.1007/s00799-015-0158-y",
            "researchField": "R135",  # Databases/Information Systems
            "contributions": [
                {
                    "name": "Contribution 1",
                    "values": {
                        "P32": [
                            {"@id": nameAmbiguityProblem}
                        ],
                        usedApproachPredicate: [
                            {"@id": heuristicRessource}
                        ],
                        similarityPredicate: [
                            {"text": 'Own defined'}
                        ],
                        followedHeuristicPredicate: [
                            {"@id": createOrFindResource(
                                'Publication features')}
                        ],
                        datasetPredicate: [
                            {"@id": createOrFindResource("DBLP")},
                            {"@id": createOrFindResource("BDBComp")},
                            {"@id": createOrFindResource("KISTI")},
                        ],
                        limitationsPredicate: [
                            {"text": "Finding optimal threshold is a difficult task."}
                        ]
                    }
                }
            ]
        }
    }

    paper = doiLookup(paper)
    response = orkg.papers.add(paper)

    print(response.content)


if __name__ == "__main__":
    main()
