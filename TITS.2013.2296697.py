from orkg import ORKG

orkg = ORKG()
vocab = dict()

vocab['orkg:yields'] = {"id": "P1", "label": "yields"}
vocab['orkg:employs'] = {"id": "P2", "label": "employs"}
vocab['orkg:problem'] = {"id": "P32", "label": "problem"}
vocab['orkg:utilizes'] = {"id": "_0001", "label": "utilizes"}
vocab['ssn:hasSubSystem'] = {"id": "_0002", "label": "sub system"}
vocab[':location'] = {"id": "_0003", "label": "location"}
vocab['rdfs:seeAlso'] = {"id": "_0004", "label": "see also"}
vocab[':manufacturer'] = {"id": "_0005", "label": "manufacturer"}
vocab[':webrosensor'] = {"id": "_0006", "label": "Webrosensor Oy"}
vocab['sosa:observes'] = {"id": "_0007", "label": "observes"}
vocab[':vibration'] = {"id": "_0008", "label": "vibration"}
vocab[':wbsDatasheet'] = {"id": "_0009", "label": "WBS CM301 Datasheet"}
vocab[':datasheet'] = {"id": "_0010", "label": "datasheet"}
vocab[':accelerometer1'] = {"id": "_0011", "label": "Accelerometer vibration sensor (sd1)"}
vocab[':accelerometer2'] = {"id": "_0012", "label": "Accelerometer vibration sensor (sd2)"}
vocab[':accelerometer3'] = {"id": "_0013", "label": "Accelerometer vibration sensor (sd3)"}
vocab[':camera1'] = {"id": "_0014", "label": "AXIS 211W Wireless Network Camera"}
vocab[':axis'] = {"id": "_0015", "label": "AXIS Communications"}
vocab[':axisDatasheet'] = {"id": "_0016", "label": "AXIS 211W Datasheet"}
vocab[':aSensorNetwork'] = {"id": "_0017", "label": "A sensor network"}
vocab[':website'] = {"id": "_0018", "label": "website"}
vocab[':STO'] = {"id": "_0019", "label": "Situation Theory Ontology"}
vocab[':WEKA'] = {"id": "_0020", "label": "Waikato Environment for Knowledge Analysis"}
vocab[':version'] = {"id": "_0021", "label": "version"}
vocab[':acronym'] = {"id": "_0022", "label": "acronym"}
vocab['cube:Observation'] = {"id": "_0023", "label": "Observation"}
vocab[':class'] = {"id": "_0024", "label": "class"}
vocab[':sensor'] = {"id": "_0025", "label": "sensor"}
vocab[':precision'] = {"id": "_0026", "label": "precision"}
vocab[':recall'] = {"id": "_0027", "label": "recall"}
vocab[':actual'] = {"id": "_0028", "label": "actual"}
vocab[':predicted'] = {"id": "_0029", "label": "predicted"}
vocab[':instances'] = {"id": "_0030", "label": "instances"}
vocab[':Jena'] = {"id": "_0031", "label": "Apache Jena"}
vocab[':SWRL'] = {"id": "_0032", "label": "Semantic Web Rule Language"}
vocab[':Cassandra'] = {"id": "_0033", "label": "Apache Cassandra"}
vocab[':Storm'] = {"id": "_0034", "label": "Apache Storm"}
vocab[':purpose'] = {"id": "_0035", "label": "purpose"}
vocab[':operation'] = {"id": "_0036", "label": "operation"}
vocab[':mode'] = {"id": "_0037", "label": "mode"}
vocab[':LabelledData'] = {"id": "_0038", "label": "Labelled Data"}
vocab[':hasPart'] = {"id": "_0039", "label": "has part"}
vocab[':value'] = {"id": "_0040", "label": "value"}
vocab[':numberOfHiddenNeurons'] = {"id": "_0041", "label": "number of hidden neurons"}
vocab[':classes'] = {"id": "_0042", "label": "classes"}
vocab[':algorithm'] = {"id": "_0043", "label": "algorithm"}
vocab[':configuration'] = {"id": "_0044", "label": "configuration"}
vocab[':evaluation'] = {"id": "_0045", "label": "evaluation"}
vocab['ssn:isPropertyOf'] = {"id": "_0046", "label": "is property of"}
vocab[':pavement'] = {"id": "_0047", "label": "pavement"}
vocab['rdfs:label'] = {"id": "_0048", "label": "label"}
vocab[':mappings'] = {"id": "_0049", "label": "class mappings"}
vocab[':rule'] = {"id": "_0050", "label": "rule"}
vocab[':performance'] = {"id": "_0051", "label": "performance"}
vocab[':time'] = {"id": "_0052", "label": "time"}


def main():
    paper = {
        "predicates": [
            {vocab['orkg:utilizes']['label']: vocab['orkg:utilizes']['id']},
            {vocab['ssn:hasSubSystem']['label']: vocab['ssn:hasSubSystem']['id']},
            {vocab[':location']['label']: vocab[':location']['id']},
            {vocab['rdfs:seeAlso']['label']: vocab['rdfs:seeAlso']['id']},
            {vocab[':manufacturer']['label']: vocab[':manufacturer']['id']},
            {vocab[':webrosensor']['label']: vocab[':webrosensor']['id']},
            {vocab['sosa:observes']['label']: vocab['sosa:observes']['id']},
            {vocab[':vibration']['label']: vocab[':vibration']['id']},
            {vocab[':wbsDatasheet']['label']: vocab[':wbsDatasheet']['id']},
            {vocab[':datasheet']['label']: vocab[':datasheet']['id']},
            {vocab[':accelerometer1']['label']: vocab[':accelerometer1']['id']},
            {vocab[':accelerometer2']['label']: vocab[':accelerometer2']['id']},
            {vocab[':accelerometer3']['label']: vocab[':accelerometer3']['id']},
            {vocab[':camera1']['label']: vocab[':camera1']['id']},
            {vocab[':axis']['label']: vocab[':axis']['id']},
            {vocab[':axisDatasheet']['label']: vocab[':axisDatasheet']['id']},
            {vocab[':aSensorNetwork']['label']: vocab[':aSensorNetwork']['id']},
            {vocab[':website']['label']: vocab[':website']['id']},
            {vocab[':STO']['label']: vocab[':STO']['id']},
            {vocab[':WEKA']['label']: vocab[':WEKA']['id']},
            {vocab[':version']['label']: vocab[':version']['id']},
            {vocab[':acronym']['label']: vocab[':acronym']['id']},
            {vocab['cube:Observation']['label']: vocab['cube:Observation']['id']},
            {vocab[':class']['label']: vocab[':class']['id']},
            {vocab[':sensor']['label']: vocab[':sensor']['id']},
            {vocab[':precision']['label']: vocab[':precision']['id']},
            {vocab[':recall']['label']: vocab[':recall']['id']},
            {vocab[':actual']['label']: vocab[':actual']['id']},
            {vocab[':predicted']['label']: vocab[':predicted']['id']},
            {vocab[':instances']['label']: vocab[':instances']['id']},
            {vocab[':Jena']['label']: vocab[':Jena']['id']},
            {vocab[':SWRL']['label']: vocab[':SWRL']['id']},
            {vocab[':Cassandra']['label']: vocab[':Cassandra']['id']},
            {vocab[':Storm']['label']: vocab[':Storm']['id']},
            {vocab[':purpose']['label']: vocab[':purpose']['id']},
            {vocab[':operation']['label']: vocab[':operation']['id']},
            {vocab[':mode']['label']: vocab[':mode']['id']},
            {vocab[':LabelledData']['label']: vocab[':LabelledData']['id']},
            {vocab[':hasPart']['label']: vocab[':hasPart']['id']},
            {vocab[':value']['label']: vocab[':value']['id']},
            {vocab[':numberOfHiddenNeurons']['label']: vocab[':numberOfHiddenNeurons']['id']},
            {vocab[':classes']['label']: vocab[':classes']['id']},
            {vocab[':algorithm']['label']: vocab[':algorithm']['id']},
            {vocab[':configuration']['label']: vocab[':configuration']['id']},
            {vocab[':evaluation']['label']: vocab[':evaluation']['id']},
            {vocab['ssn:isPropertyOf']['label']: vocab['ssn:isPropertyOf']['id']},
            {vocab[':pavement']['label']: vocab[':pavement']['id']},
            {vocab['rdfs:label']['label']: vocab['rdfs:label']['id']},
            {vocab[':mappings']['label']: vocab[':mappings']['id']},
            {vocab[':rule']['label']: vocab[':rule']['id']},
            {vocab[':performance']['label']: vocab[':performance']['id']},
            {vocab[':time']['label']: vocab[':time']['id']}
        ],
        "paper": {
            "title": "Situational Knowledge Representation for Traffic Observed by a Pavement Vibration Sensor Network",
            "doi": "10.1109/TITS.2013.2296697",
            "authors": [{"label": "Markus Stocker"}, {"label": "Mauno Rönkkö"}, {"label": "Mikko Kolehmainen"}],
            "publicationMonth": 2,
            "publicationYear": "2014",
            "researchField": "R278",
            "contributions": [
                {
                    "name": "Detection",
                    "values": {
                        vocab['orkg:problem']['id']: [{"label": "Road vehicle detection"}],
                        vocab['orkg:utilizes']['id']: [
                            {
                                "@temp": vocab[':aSensorNetwork']['id'],
                                "label": vocab[':aSensorNetwork']['label'],
                                "values": {
                                    vocab[':location']['id']: [
                                        {
                                            "label": "Emergency Services Academy Finland Training Ground",
                                            "values": {
                                                vocab['rdfs:seeAlso']['id']: [
                                                    {
                                                        "text": "https://www.pelastusopisto.fi/en/learning-environments/the-training-ground/"
                                                    }
                                                ]
                                            }
                                        }
                                    ],
                                    vocab['ssn:hasSubSystem']['id']: [
                                        {
                                            "@temp": vocab[':accelerometer1']['id'],
                                            "label": vocab[':accelerometer1']['label'],
                                            "values": {
                                                vocab[':manufacturer']['id']: [
                                                    {
                                                        "@temp": vocab[':webrosensor']['id'],
                                                        "label": vocab[':webrosensor']['label'],
                                                        "values": {
                                                            vocab[':website']['id']: [
                                                                {
                                                                    "text": "http://webrosensor.fi"
                                                                }
                                                            ]
                                                        }
                                                    }
                                                ],
                                                vocab['sosa:observes']['id']: [
                                                    {
                                                        "@temp": vocab[':vibration']['id'],
                                                        "label": vocab[':vibration']['label'],
                                                        "values": {
                                                            vocab['ssn:isPropertyOf']['id']: [
                                                                {
                                                                    "label": vocab[':pavement']['label']
                                                                }
                                                            ]
                                                        }
                                                    }
                                                ],
                                                vocab[':datasheet']['id']: [
                                                    {
                                                        "@temp": vocab[':wbsDatasheet']['id'],
                                                        "label": vocab[':wbsDatasheet']['label'],
                                                        "values": {
                                                            vocab['rdfs:seeAlso']['id']: [
                                                                {
                                                                    "text": "https://www.sensor-test.de/ausstellerbereich/upload/mnpdf/en/WBS_CM301_Datasheet_0410_web.pdf"
                                                                }
                                                            ]
                                                        }
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "@temp": vocab[':accelerometer2']['id'],
                                            "label": vocab[':accelerometer2']['label'],
                                            "values": {
                                                vocab[':manufacturer']['id']: [{"@id": vocab[':webrosensor']['id']}],
                                                vocab['sosa:observes']['id']: [{"@id": vocab[':vibration']['id']}],
                                                vocab[':datasheet']['id']: [{"@id": vocab[':wbsDatasheet']['id']}]
                                            }
                                        },
                                        {
                                            "@temp": vocab[':accelerometer3']['id'],
                                            "label": vocab[':accelerometer3']['label'],
                                            "values": {
                                                vocab[':manufacturer']['id']: [{"@id": vocab[':webrosensor']['id']}],
                                                vocab['sosa:observes']['id']: [{"@id": vocab[':vibration']['id']}],
                                                vocab[':datasheet']['id']: [{"@id": vocab[':wbsDatasheet']['id']}]
                                            }
                                        },
                                        {
                                            "@temp": vocab[':camera1']['id'],
                                            "label": vocab[':camera1']['label'],
                                            "values": {
                                                vocab[':manufacturer']['id']: [
                                                    {
                                                        "@temp": vocab[':axis']['id'],
                                                        "label": vocab[':axis']['label'],
                                                        "values": {
                                                            vocab[':website']['id']: [
                                                                {
                                                                    "text": "https://www.axis.com"
                                                                }
                                                            ]
                                                        }
                                                    }
                                                ],
                                                vocab[':datasheet']['id']: [
                                                    {
                                                        "@temp": vocab[':axisDatasheet']['id'],
                                                        "label": vocab[':axisDatasheet']['label'],
                                                        "values": {
                                                            vocab['rdfs:seeAlso']['id']: [
                                                                {
                                                                    "text": "https://www.axis.com/files/datasheet/ds_211w_42335_en_1103_lo.pdf"
                                                                }
                                                            ]
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "label": "Semantic Sensor Network Ontology",
                                "values": {
                                    vocab['rdfs:seeAlso']['id']: [
                                        {"text": "http://purl.oclc.org/NET/ssnx/ssn"},
                                        {"text": "https://www.w3.org/TR/vocab-ssn/"}
                                    ]
                                }
                            },
                            {
                                "@temp": vocab[':WEKA']['id'],
                                "label": vocab[':WEKA']['label'],
                                "values": {
                                    vocab['rdfs:seeAlso']['id']: [
                                        {"text": "https://www.cs.waikato.ac.nz/ml/weka/"}
                                    ],
                                    vocab[':version']['id']: [
                                        {"text": "3.6.5"}
                                    ],
                                    vocab[':acronym']['id']: [
                                        {"text": "WEKA"}
                                    ]
                                }
                            },
                            {
                                "@temp": vocab[':Cassandra']['id'],
                                "label": vocab[':Cassandra']['label'],
                                "values": {
                                    vocab['rdfs:seeAlso']['id']: [
                                        {"text": "http://cassandra.apache.org"}
                                    ]
                                }
                            },
                            {
                                "@temp": vocab[':LabelledData']['id'],
                                "label": vocab[':LabelledData']['label'],
                                "values": {
                                    vocab[':hasPart']['id']: [
                                        {"text": "Vehicle occurrences"},
                                        {
                                            "label": "Vehicle type",
                                            "values": {
                                                vocab[':hasPart']['id']: [
                                                    {"text": "mini-cleaner"},
                                                    {"text": "mini-lifter"},
                                                    {"text": "personal-car"},
                                                    {"text": "van"},
                                                    {"text": "ambulance"},
                                                    {"text": "fire-van"},
                                                    {"text": "pickup-truck"},
                                                    {"text": "truck"},
                                                    {"text": "fire-truck"},
                                                    {"text": "bucket-digger"}
                                                ]
                                            }
                                        },
                                        {"text": "Time at which the vehicle crossed the approximate location of sensor sd2"},
                                        {"text": "Driving side of the vehicle"}
                                    ]
                                }
                            },
                            {
                                "label": "Training Data",
                                "values": {
                                    vocab['rdfs:label']['id']: [
                                        {"text":"Key characteristics of the datasets used to train the classifier for the machine learning vehicle detection task with training classes vehicle and no-vehicle"}
                                    ],
                                    vocab[':mappings']['id']: [
                                        {
                                            "label": "vehicle",
                                            "values": {
                                                vocab['rdfs:label']['id']: [
                                                    {"text":"Mapping of vehicle labels (e.g., personal-car) to the vehicle training class"}
                                                ],
                                                vocab[':hasPart']['id']: [
                                                    {"text": "mini-cleaner"},
                                                    {"text": "mini-lifter"},
                                                    {"text": "personal-car"},
                                                    {"text": "van"},
                                                    {"text": "ambulance"},
                                                    {"text": "fire-van"},
                                                    {"text": "pickup-truck"},
                                                    {"text": "truck"},
                                                    {"text": "fire-truck"},
                                                    {"text": "bucket-digger"}
                                                ]
                                            }
                                        },
                                        {
                                            "label": "no-vehicle",
                                            "values": {
                                                vocab['rdfs:label']['id']: [
                                                    {"text": "Mapping of no-vehicle labels (e.g., background vibration) to the no-vehicle training class"}
                                                ],
                                                vocab[':hasPart']['id']: [
                                                    {"text": "background"},
                                                    {"text": "no-vehicle"}
                                                ]
                                            }
                                        }
                                    ],
                                    vocab['cube:Observation']['id']: [
                                        {
                                            "label": "Class: vehicle; Sensor: sd1",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':instances']['id']: [{"text": "87"}]
                                            }
                                        },
                                        {
                                            "label": "Class: vehicle; Sensor: sd2",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':instances']['id']: [{"text": "134"}]
                                            }
                                        },
                                        {
                                            "label": "Class: vehicle; Sensor: sd3",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':instances']['id']: [{"text": "133"}]
                                            }
                                        },
                                        {
                                            "label": "Class: no-vehicle; Sensor: sd1",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "no-vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':instances']['id']: [{"text": "345"}]
                                            }
                                        },
                                        {
                                            "label": "Class: no-vehicle; Sensor: sd2",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "no-vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':instances']['id']: [{"text": "176"}]
                                            }
                                        },
                                        {
                                            "label": "Class: no-vehicle; Sensor: sd3",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "no-vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':instances']['id']: [{"text": "166"}]
                                            }
                                        }
                                    ]
                                }
                            }
                        ],
                        vocab['orkg:employs']['id']: [
                            {
                                "label": "Digital Signal Processing Operations",
                                "values": {
                                    vocab[':purpose']['id']: [
                                        {"text": "Iteratively process vibration data into vibration patterns"}
                                    ],
                                    vocab[':mode']['id']: [
                                        {"text": "Automated"}
                                    ],
                                    vocab[':operation']['id']: [
                                        {
                                            "label": "Bandpass filter",
                                            "values": {
                                                vocab[':purpose']['id']: [
                                                    {"text": "Enhance vibration signal possibly induced by vehicles"}
                                                ]
                                            }
                                        },
                                        {
                                            "label": "Fourier transform",
                                            "values": {
                                                vocab[':purpose']['id']: [
                                                    {"text": "Transform filtered sensor data into a vibration pattern"}
                                                ]
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "label": "Machine Learning",
                                "values": {
                                    vocab[':algorithm']['id']: [
                                        {
                                            "label": "Multi-Layer Perceptron Neural Network",
                                            "values": {
                                                vocab[':acronym']['id']: [
                                                    {"text": "MLP"}
                                                ]
                                            }
                                        }
                                    ],
                                    vocab[':configuration']['id']: [
                                        {
                                            "label": "number of input neurons",
                                            "values": {
                                                vocab[':value']['id']: [
                                                    {"text": "10"}
                                                ]
                                            }
                                        },
                                        {
                                            "label": "number of output neurons",
                                            "values": {
                                                vocab[':value']['id']: [
                                                    {"text": "2"}
                                                ]
                                            }
                                        },
                                        {
                                            "label": "learning rate",
                                            "values": {
                                                vocab[':value']['id']: [
                                                    {"text": "0.3"}
                                                ]
                                            }
                                        },
                                        {
                                            "label": "momentum",
                                            "values": {
                                                vocab[':value']['id']: [
                                                    {"text": "0.2"}
                                                ]
                                            }
                                        },
                                        {
                                            "label": "hidden layers",
                                            "values": {
                                                vocab[':hasPart']['id']: [
                                                    {
                                                        "label": "Hidden Layer",
                                                        "values": {
                                                            vocab[':numberOfHiddenNeurons']['id']: [
                                                                {"text": "6"}
                                                            ]
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    ],
                                    vocab[':evaluation']['id']: [
                                        {"text": "Tenfold cross validation"}
                                    ],
                                    vocab[':classes']['id']: [
                                        {"label": "vehicle"},
                                        {"label": "no-vehicle"}
                                    ]
                                }
                            }
                        ],
                        vocab['orkg:yields']['id']: [
                            {
                                "label": "Classification performance for the three sensors (sd1, sd2, sd3)",
                                "values": {
                                    vocab['rdfs:label']['id']: [
                                        {"text":"Percent of correctly classified instances"}
                                    ],
                                    vocab['cube:Observation']['id']: [
                                        {
                                            "label": "Sensor: sd1",
                                            "values": {
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':value']['id']: [{"text": "92%"}]
                                            }
                                        },
                                        {
                                            "label": "Sensor: sd2",
                                            "values": {
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':value']['id']: [{"text": "95%"}]
                                            }
                                        },
                                        {
                                            "label": "Sensor: sd3",
                                            "values": {
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':value']['id']: [{"text": "96%"}]
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "label": "Summary of precision and recall figures for the no-vehicle and vehicle classes of the vehicle detection task for the three sensors (sd1, sd2, sd3)",
                                "values": {
                                    vocab['cube:Observation']['id']: [
                                        {
                                            "label": "Class: no-vehicle; Sensor: sd1",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "no-vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':precision']['id']: [{"text": "0.967"}],
                                                vocab[':recall']['id']: [{"text": "0.933"}]
                                            }
                                        },
                                        {
                                            "label": "Class: no-vehicle; Sensor: sd2",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "no-vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':precision']['id']: [{"text": "0.971"}],
                                                vocab[':recall']['id']: [{"text": "0.943"}]
                                            }
                                        },
                                        {
                                            "label": "Class: no-vehicle; Sensor: sd3",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "no-vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':precision']['id']: [{"text": "0.953"}],
                                                vocab[':recall']['id']: [{"text": "0.97"}]
                                            }
                                        },
                                        {
                                            "label": "Class: vehicle; Sensor: sd1",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':precision']['id']: [{"text": "0.768"}],
                                                vocab[':recall']['id']: [{"text": "0.874"}]
                                            }
                                        },
                                        {
                                            "label": "Class: vehicle; Sensor: sd2",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':precision']['id']: [{"text": "0.928"}],
                                                vocab[':recall']['id']: [{"text": "0.963"}]
                                            }
                                        },
                                        {
                                            "label": "Class: vehicle; Sensor: sd3",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':precision']['id']: [{"text": "0.962"}],
                                                vocab[':recall']['id']: [{"text": "0.94"}]
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "label": "Summary of the confusion matrices for the vehicle detection task for the three sensors (sd1, sd2, sd3)",
                                "values": {
                                    vocab['cube:Observation']['id']: [
                                        {
                                            "label": "Actual: no-vehicle; Predicted: no-vehicle; Sensor: sd1",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "no-vehicle"}],
                                                vocab[':predicted']['id']: [{"text": "no-vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':instances']['id']: [{"text": "322"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: no-vehicle; Predicted: vehicle; Sensor: sd1",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "no-vehicle"}],
                                                vocab[':predicted']['id']: [{"text": "vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':instances']['id']: [{"text": "23"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: vehicle; Predicted: no-vehicle; Sensor: sd1",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "vehicle"}],
                                                vocab[':predicted']['id']: [{"text": "no-vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':instances']['id']: [{"text": "11"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: vehicle; Predicted: vehicle; Sensor: sd1",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "vehicle"}],
                                                vocab[':predicted']['id']: [{"text": "vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':instances']['id']: [{"text": "76"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: no-vehicle; Predicted: no-vehicle; Sensor: sd2",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "no-vehicle"}],
                                                vocab[':predicted']['id']: [{"text": "no-vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':instances']['id']: [{"text": "166"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: no-vehicle; Predicted: vehicle; Sensor: sd2",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "no-vehicle"}],
                                                vocab[':predicted']['id']: [{"text": "vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':instances']['id']: [{"text": "10"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: vehicle; Predicted: no-vehicle; Sensor: sd2",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "vehicle"}],
                                                vocab[':predicted']['id']: [{"text": "no-vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':instances']['id']: [{"text": "5"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: vehicle; Predicted: vehicle; Sensor: sd2",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "vehicle"}],
                                                vocab[':predicted']['id']: [{"text": "vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':instances']['id']: [{"text": "129"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: no-vehicle; Predicted: no-vehicle; Sensor: sd3",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "no-vehicle"}],
                                                vocab[':predicted']['id']: [{"text": "no-vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':instances']['id']: [{"text": "161"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: no-vehicle; Predicted: vehicle; Sensor: sd3",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "no-vehicle"}],
                                                vocab[':predicted']['id']: [{"text": "vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':instances']['id']: [{"text": "5"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: vehicle; Predicted: no-vehicle; Sensor: sd3",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "vehicle"}],
                                                vocab[':predicted']['id']: [{"text": "no-vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':instances']['id']: [{"text": "8"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: vehicle; Predicted: vehicle; Sensor: sd3",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "vehicle"}],
                                                vocab[':predicted']['id']: [{"text": "vehicle"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':instances']['id']: [{"text": "125"}]
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                },
                {
                    "name": "Classification",
                    "values": {
                        vocab['orkg:problem']['id']: [{"label": "Road vehicle classification"}],
                        vocab['orkg:utilizes']['id']: [
                            {
                                "@id": vocab[':aSensorNetwork']['id']
                            },
                            {
                                "@id": vocab[':WEKA']['id']
                            },
                            {
                                "label": "Training Data",
                                "values": {
                                    vocab['rdfs:label']['id']: [
                                        {
                                            "text": "Key characteristics of the datasets used to train the classifier for the machine learning vehicle classification task with training classes light and heavy"}
                                    ],
                                    vocab[':mappings']['id']: [
                                        {
                                            "label": "light",
                                            "values": {
                                                vocab['rdfs:label']['id']: [
                                                    {
                                                        "text": "Mapping of vehicle labels (e.g., personal-car) to the light training class"}
                                                ],
                                                vocab[':hasPart']['id']: [
                                                    {"text": "mini-cleaner"},
                                                    {"text": "mini-lifter"},
                                                    {"text": "van"},
                                                    {"text": "ambulance"},
                                                    {"text": "fire-van"},
                                                    {"text": "pickup-truck"}
                                                ]
                                            }
                                        },
                                        {
                                            "label": "heavy",
                                            "values": {
                                                vocab['rdfs:label']['id']: [
                                                    {
                                                        "text": "Mapping of vehicle labels (e.g., truck) to the heavy training class"}
                                                ],
                                                vocab[':hasPart']['id']: [
                                                    {"text": "truck"},
                                                    {"text": "fire-truck"},
                                                    {"text": "bucket-digger"}
                                                ]
                                            }
                                        }
                                    ],
                                    vocab['cube:Observation']['id']: [
                                        {
                                            "label": "Class: light; Sensor: sd1",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':instances']['id']: [{"text": "40"}]
                                            }
                                        },
                                        {
                                            "label": "Class: light; Sensor: sd2",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':instances']['id']: [{"text": "71"}]
                                            }
                                        },
                                        {
                                            "label": "Class: light; Sensor: sd3",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':instances']['id']: [{"text": "71"}]
                                            }
                                        },
                                        {
                                            "label": "Class: heavy; Sensor: sd1",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':instances']['id']: [{"text": "47"}]
                                            }
                                        },
                                        {
                                            "label": "Class: heavy; Sensor: sd2",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':instances']['id']: [{"text": "63"}]
                                            }
                                        },
                                        {
                                            "label": "Class: heavy; Sensor: sd3",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':instances']['id']: [{"text": "62"}]
                                            }
                                        }
                                    ]
                                }
                            }
                        ],
                        vocab['orkg:employs']['id']: [
                            {
                                "label": "Machine Learning",
                                "values": {
                                    vocab[':algorithm']['id']: [
                                        {
                                            "label": "Multilayer perceptron feedforward artificial neural network",
                                            "values": {
                                                vocab[':acronym']['id']: [
                                                    {"text": "MLP"}
                                                ]
                                            }
                                        }
                                    ],
                                    vocab[':purpose']['id']: [
                                        {"text": "Classify vibration patterns"}
                                    ],
                                    vocab[':configuration']['id']: [
                                        {
                                            "label": "number of input neurons",
                                            "values": {
                                                vocab[':value']['id']: [
                                                    {"text": "10"}
                                                ]
                                            }
                                        },
                                        {
                                            "label": "number of output neurons",
                                            "values": {
                                                vocab[':value']['id']: [
                                                    {"text": "2"}
                                                ]
                                            }
                                        },
                                        {
                                            "label": "learning rate",
                                            "values": {
                                                vocab[':value']['id']: [
                                                    {"text": "0.3"}
                                                ]
                                            }
                                        },
                                        {
                                            "label": "momentum",
                                            "values": {
                                                vocab[':value']['id']: [
                                                    {"text": "0.2"}
                                                ]
                                            }
                                        },
                                        {
                                            "label": "hidden layers",
                                            "values": {
                                                vocab[':hasPart']['id']: [
                                                    {
                                                        "label": "Hidden Layer",
                                                        "values": {
                                                            vocab[':numberOfHiddenNeurons']['id']: [
                                                                {"text": "6"}
                                                            ]
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    ],
                                    vocab[':evaluation']['id']: [
                                        {"text": "Tenfold cross validation"}
                                    ],
                                    vocab[':classes']['id']: [
                                        {"label": "light"},
                                        {"label": "heavy"}
                                    ]
                                }
                            }
                        ],
                        vocab['orkg:yields']['id']: [
                            {
                                "label": "Classification performance for the three sensors (sd1, sd2, sd3)",
                                "values": {
                                    vocab['rdfs:label']['id']: [
                                        {"text": "Percent of correctly classified instances"}
                                    ],
                                    vocab['cube:Observation']['id']: [
                                        {
                                            "label": "Sensor: sd1",
                                            "values": {
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':value']['id']: [{"text": "82%"}]
                                            }
                                        },
                                        {
                                            "label": "Sensor: sd2",
                                            "values": {
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':value']['id']: [{"text": "75%"}]
                                            }
                                        },
                                        {
                                            "label": "Sensor: sd3",
                                            "values": {
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':value']['id']: [{"text": "83%"}]
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "label": "Summary of precision and recall figures for the light and heavy classes of the vehicle classification task for the three sensors (sd1, sd2, sd3)",
                                "values": {
                                    vocab['cube:Observation']['id']: [
                                        {
                                            "label": "Class: light; Sensor sd1",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':precision']['id']: [{"text": "0.8"}],
                                                vocab[':recall']['id']: [{"text": "0.8"}]
                                            }
                                        },
                                        {
                                            "label": "Class: light; Sensor: sd2",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':precision']['id']: [{"text": "0.788"}],
                                                vocab[':recall']['id']: [{"text": "0.732"}]
                                            }
                                        },
                                        {
                                            "label": "Class: light; Sensor: sd3",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':precision']['id']: [{"text": "0.816"}],
                                                vocab[':recall']['id']: [{"text": "0.873"}]
                                            }
                                        },
                                        {
                                            "label": "Class: heavy; Sensor: sd1",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':precision']['id']: [{"text": "0.83"}],
                                                vocab[':recall']['id']: [{"text": "0.83"}]
                                            }
                                        },
                                        {
                                            "label": "Class: heavy; Sensor: sd2",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':precision']['id']: [{"text": "0.721"}],
                                                vocab[':recall']['id']: [{"text": "0.778"}]
                                            }
                                        },
                                        {
                                            "label": "Class: heavy; Sensor: sd3",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':precision']['id']: [{"text": "0.842"}],
                                                vocab[':recall']['id']: [{"text": "0.774"}]
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "label": "Summary of the confusion matrices for the vehicle classification task for the three sensors (sd1, sd2, sd3)",
                                "values": {
                                    vocab['cube:Observation']['id']: [
                                        {
                                            "label": "Actual: heavy; Predicted: heavy; Sensor: sd1",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "heavy"}],
                                                vocab[':predicted']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':instances']['id']: [{"text": "39"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: heavy; Predicted: light; Sensor: sd1",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "heavy"}],
                                                vocab[':predicted']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':instances']['id']: [{"text": "8"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: light; Predicted: heavy; Sensor: sd1",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "light"}],
                                                vocab[':predicted']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':instances']['id']: [{"text": "8"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: light; Predicted: light; Sensor: sd1",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "light"}],
                                                vocab[':predicted']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':instances']['id']: [{"text": "32"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: heavy; Predicted: heavy; Sensor: sd2",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "heavy"}],
                                                vocab[':predicted']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':instances']['id']: [{"text": "49"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: heavy; Predicted: light; Sensor: sd2",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "heavy"}],
                                                vocab[':predicted']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':instances']['id']: [{"text": "14"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: light; Predicted: heavy; Sensor: sd2",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "light"}],
                                                vocab[':predicted']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':instances']['id']: [{"text": "19"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: light; Predicted: light; Sensor: sd2",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "light"}],
                                                vocab[':predicted']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':instances']['id']: [{"text": "52"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: heavy; Predicted: heavy; Sensor: sd3",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "heavy"}],
                                                vocab[':predicted']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':instances']['id']: [{"text": "48"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: heavy; Predicted: light; Sensor: sd3",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "heavy"}],
                                                vocab[':predicted']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':instances']['id']: [{"text": "14"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: light; Predicted: heavy; Sensor: sd3",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "light"}],
                                                vocab[':predicted']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':instances']['id']: [{"text": "9"}]
                                            }
                                        },
                                        {
                                            "label": "Actual: light; Predicted: light; Sensor: sd3",
                                            "values": {
                                                vocab[':actual']['id']: [{"text": "light"}],
                                                vocab[':predicted']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':instances']['id']: [{"text": "62"}]
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                },
                {
                    "name": "Knowledge Representation",
                    "values": {
                        vocab['orkg:problem']['id']: [{"label": "Traffic situation knowledge representation"}],
                        vocab['orkg:utilizes']['id']: [
                            {
                                "@temp": vocab[':STO']['id'],
                                "label": vocab[':STO']['label'],
                                "values": {
                                    vocab['rdfs:seeAlso']['id']: [
                                        {"text": "http://vistology.com/ont/STO/STO.owl"}
                                    ],
                                    vocab[':purpose']['id']: [
                                        {"text": "Represent knowledge about real-world situations involving vehicles and their type, speed and driving direction"}
                                    ]
                                }
                            },
                            {
                                "@temp": vocab[':Jena']['id'],
                                "label": vocab[':Jena']['label'],
                                "values": {
                                    vocab['rdfs:seeAlso']['id']: [
                                        {"text": "http://incubator.apache.org/jena/"}
                                    ],
                                    vocab[':version']['id']: [
                                        {"text": "2.7.0"}
                                    ],
                                }
                            },
                            {
                                "@temp": vocab[':Storm']['id'],
                                "label": vocab[':Storm']['label'],
                                "values": {
                                    vocab['rdfs:seeAlso']['id']: [
                                        {"text": "https://storm.apache.org/"}
                                    ]
                                }
                            }
                        ],
                        vocab['orkg:employs']['id']: [
                            {
                                "label": "Situation representation",
                                "values": {
                                    vocab['rdfs:label']['id']: [
                                        {"text": "A situation supports a near-relation infon with parameters anchored to a specific vehicle, accelerometer sensor, spatial and temporal locations"}
                                    ],
                                    vocab[':hasPart']['id']: [
                                        {
                                            "label": "Infon",
                                            "values": {
                                                vocab[':hasPart']['id']: [
                                                    {
                                                        "label": "Relation",
                                                        "values": {
                                                            vocab[':hasPart']['id']: [
                                                                {"text": "near"}
                                                            ]
                                                        }
                                                    },
                                                    {
                                                        "label": "Relevant Individuals",
                                                        "values": {
                                                            vocab[':hasPart']['id']: [
                                                                {"text": "vehicle"},
                                                                {"text": "accelerometer sensor"}
                                                            ]
                                                        }
                                                    },
                                                    {
                                                        "label": "Attributes",
                                                        "values": {
                                                            vocab[':hasPart']['id']: [
                                                                {"text": "spatial location"},
                                                                {"text": "temporal location"}
                                                            ]
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            },
                        ],
                        vocab['orkg:yields']['id']: [
                            {
                                "label": "Situations representing classified vehicle occurrences near sensors",
                                "values": {
                                    vocab['cube:Observation']['id']: [
                                        {
                                            "label": "Situation light vehicle near sd1 at 14:50:36",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':time']['id']: [{"text": "14:50:36"}]
                                            }
                                        },
                                        {
                                            "label": "Situation light vehicle near sd2 at 14:50:39",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':time']['id']: [{"text": "14:50:39"}]
                                            }
                                        },
                                        {
                                            "label": "Situation heavy vehicle near sd3 at 14:50:43",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':time']['id']: [{"text": "14:50:43"}]
                                            }
                                        },
                                        {
                                            "label": "Situation light vehicle near sd1 at 14:53:20",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':time']['id']: [{"text": "14:53:20"}]
                                            }
                                        },
                                        {
                                            "label": "Situation light vehicle near sd2 at 14:53:24",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':time']['id']: [{"text": "14:53:24"}]
                                            }
                                        },
                                        {
                                            "label": "Situation heavy vehicle near sd3 at 14:53:27",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':time']['id']: [{"text": "14:53:27"}]
                                            }
                                        },
                                        {
                                            "label": "Situation heavy vehicle near sd1 at 14:57:56",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':time']['id']: [{"text": "14:57:56"}]
                                            }
                                        },
                                        {
                                            "label": "Situation heavy vehicle near sd2 at 14:57:52",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':time']['id']: [{"text": "14:57:52"}]
                                            }
                                        },
                                        {
                                            "label": "Situation heavy vehicle near sd3 at 14:57:49",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':time']['id']: [{"text": "14:57:49"}]
                                            }
                                        },
                                        {
                                            "label": "Situation heavy vehicle near sd1 at 14:58:39",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd1"}],
                                                vocab[':time']['id']: [{"text": "14:58:39"}]
                                            }
                                        },
                                        {
                                            "label": "Situation light vehicle near sd2 at 14:58:43",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "light"}],
                                                vocab[':sensor']['id']: [{"text": "sd2"}],
                                                vocab[':time']['id']: [{"text": "14:58:43"}]
                                            }
                                        },
                                        {
                                            "label": "Situation heavy vehicle near sd3 at 14:58:46",
                                            "values": {
                                                vocab[':class']['id']: [{"text": "heavy"}],
                                                vocab[':sensor']['id']: [{"text": "sd3"}],
                                                vocab[':time']['id']: [{"text": "14:58:46"}]
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                },
                {
                    "name": "Reasoning",
                    "values": {
                        vocab['orkg:problem']['id']: [{"label": "Traffic situation reasoning"}],
                        vocab['orkg:utilizes']['id']: [
                            {
                                "@id": vocab[':STO']['id']
                            },
                            {
                                "@id": vocab[':Jena']['id']
                            },
                            {
                                "@temp": vocab[':SWRL']['id'],
                                "label": vocab[':SWRL']['label'],
                                "values": {
                                    vocab['rdfs:seeAlso']['id']: [
                                        {"text": "https://www.w3.org/Submission/SWRL/"}
                                    ],
                                    vocab[':acronym']['id']: [
                                        {"text": "SWRL"}
                                    ]
                                }
                            }
                        ],
                        vocab['orkg:employs']['id']: [
                            {
                                "label": "Situation representation",
                                "values": {
                                    vocab[':rule']['id']: [
                                        {"text": "Vehicles are same physical object if near different sensors within 8 seconds"},
                                        {"text": "Infer the velocity of vehicles by representing infons with driving-side and driving-speed relation"}
                                    ]
                                }
                            }
                        ],
                        vocab['orkg:yields']['id']: [
                            {
                                "label": "Infons for driving side and speed as well as same as relations among vehicles inferred according to the two rules"
                            }
                        ]
                    }
                }
            ]
        }
    }

    response = orkg.papers.add(paper)

    print(str(response.content))


if __name__ == "__main__":
    main()
